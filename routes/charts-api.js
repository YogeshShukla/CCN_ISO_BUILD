const router = require('express').Router();
const elasticSearch = require('elasticsearch');
//const params = require('./request-params');


const index = {
    infrastructure: "metricbeat-*",
    applications: "metricbeat-*",
    financials: "aws-billing-*",
    awsconfig: "",
    audit: "auditbeat-*",
    ospatches: "patchdata"
}
// const host = {
//     infrastructure: "54.84.12.123:9200",
//     applications: "54.84.12.123:9200",
//     financials: "54.172.179.105:9200",
//     awsconfig: "search-aws-config-es-c36loymif24bmtkk5rrdmemqo4.us-east-1.es.amazonaws.com",
//     audit: "54.84.12.123:9200",
//     ospatches:"54.84.12.123:9200"

// }

// const host = {
//     infrastructure: "34.2.48.173:9200",
//     applications: "34.232.48.173:9200",
//     financials: "54.172.179.105:9200",
//     awsconfig: "search-es-config-ayexgcwc3dp5qurypdqxw4bdmm.us-east-1.es.amazonaws.com",
//     audit: "54.175.196.189:9200"
// }

const host = {
    infrastructure: "34.200.246.73:9200",
    applications: "34.200.246.73:9200",
    financials: "54.172.179.105:9200",
    awsconfig: "search-es-config-ayexgcwc3dp5qurypdqxw4bdmm.us-east-1.es.amazonaws.com",
    audit: "34.200.246.73:9200",
    ospatches:"34.200.246.73:9200",

}

const rangePath = {
    infrastructure: "",
    applications: "@timestamp",
    financials: "lineItem/UsageStartDate",
    awsconfig: "snapshotTimeIso",
    audit: "@timestamp",
    ospatches: ""
}

let searchObj = {};


router.post('/', prepareSearchObj, (req, res) => {

    const elasticClient = new elasticSearch.Client({
        host: host[req.pathname],
        log: 'trace'
    });

    elasticClient.search(searchObj).then((body) => {
        res.json(body);
    }, function (error) {
        console.trace(error.message);
    });
});


function prepareSearchObj(req, res, next) {

    const getMethodTabs = ["infrastructure","ospatches"];
    const postMethodTabs = ["applications", "financials", "awsconfig", "audit"];
    let baseUrlArray = req.baseUrl.split('/');

    let pathname = baseUrlArray[baseUrlArray.length - 1];
    req.pathname = pathname;
    if (getMethodTabs.indexOf(pathname) != -1) {
        searchObj = {
            index: index[pathname],
            q: req.body.target
        }

    } else {

        const params = require('./params/' + pathname + '-params');


        let bodyParams = params[req.body.target];

        if (rangePath[pathname] != '') {
            let rengeIndex = 0;
            let i = 0;
            bodyParams.query.bool.must.forEach(item => {
                if (item.range != undefined) {
                    rengeIndex = i;
                }
                i++;
            })

            bodyParams.query.bool.must[rengeIndex]['range'][rangePath[pathname]]['gte'] = req.body.startDate;
            bodyParams.query.bool.must[rengeIndex]['range'][rangePath[pathname]]['lte'] = req.body.endDate;

        }


        searchObj = {
            index: index[pathname],
            body: bodyParams
        }
    }

    return next();
}

module.exports = router;