const request = require('request');
const router = require('express').Router();
const osPatchesUri = 'http://34.200.246.73:9200/patchdata/_search';

const osPatchesHeaders = {
    'Content-Type': 'application/json',
    'kbn-xsrf': 'reporting'
};
router.get('/', prepareSearchObj, (req, res) => {
    request({
        headers: osPatchesHeaders,
        uri: osPatchesUri,
        //body: stringifyData,
        method: 'GET'
    }, (err, httpres, body) => {
        res.json(httpres);
    });
});


function prepareSearchObj(req, res, next) {
//     let trendingParams = params[req.body.target];

//     var minTime = new Date();
//     minTime.setMinutes( minTime.getMinutes() - 15 );

//    trendingParams.timerange.min = (minTime).toISOString();
//    trendingParams.timerange.max = new Date().toISOString();


//     stringifyData = JSON.stringify(trendingParams);
    return next();
}

module.exports = router;
