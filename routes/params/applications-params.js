module.exports = {
    cpuUsage: {
        "highlight": {
          "pre_tags": [
            "@kibana-highlighted-field@"
          ],
          "post_tags": [
            "@/kibana-highlighted-field@"
          ],
          "fields": {
            "*": {}
          },
          "fragment_size": 2147483647
        },
        "size": 0,
        "_source": {
          "excludes": []
        },
        "aggs": {
          "2": {
            "date_histogram": {
              "field": "@timestamp",
              "interval": "12h",
              "time_zone": "Asia/Kolkata",
              "min_doc_count": 1
            },
            "aggs": {
              "3": {
                "terms": {
                  "field": "docker.container.name",
                  "size": 5,
                  "order": {
                    "_term": "desc"
                  }
                }
              }
            }
          }
        },
        "version": true,
        "stored_fields": [
          "*"
        ],
        "script_fields": {},
        "docvalue_fields": [
          "@timestamp",
          "ceph.monitor_health.last_updated",
          "docker.container.created",
          "docker.healthcheck.event.end_date",
          "docker.healthcheck.event.start_date",
          "docker.image.created",
          "kubernetes.container.start_time",
          "kubernetes.event.metadata.timestamp.created",
          "kubernetes.node.start_time",
          "kubernetes.pod.start_time",
          "kubernetes.system.start_time",
          "mongodb.status.background_flushing.last_finished",
          "mongodb.status.local_time",
          "php_fpm.pool.start_time",
          "postgresql.activity.backend_start",
          "postgresql.activity.query_start",
          "postgresql.activity.state_change",
          "postgresql.activity.transaction_start",
          "postgresql.bgwriter.stats_reset",
          "postgresql.database.stats_reset",
          "system.process.cpu.start_time"
        ],
        "query": {
          "bool": {
            "must": [
              {
                "query_string": {
                  "query": "metricset.module:docker AND metricset.name:cpu",
                  "analyze_wildcard": true,
                  "default_field": "*"
                }
              },
              {
                "query_string": {
                  "analyze_wildcard": true,
                  "default_field": "*",
                  "query": "*"
                }
              },
              {
                "range": {
                  "@timestamp": {
                    "gte": 1525252386940,
                    "lte": 1527844386941,
                    "format": "epoch_millis"
                  }
                }
              }
            ],
            "filter": [],
            "should": [],
            "must_not": []
          }
        }
      },
    containerCount: {
        "size": 0,
        "aggs": {
          "2": {
            "max": {
              "field": "docker.info.containers.running"
            }
          },
          "3": {
            "max": {
              "field": "docker.info.containers.paused"
            }
          },
          "4": {
            "max": {
              "field": "docker.info.containers.stopped"
            }
          }
        },
        "highlight": {
          "pre_tags": [
            "@kibana-highlighted-field@"
          ],
          "post_tags": [
            "@/kibana-highlighted-field@"
          ],
          "fields": {
            "*": {}
          },
          "fragment_size": 2147483647
        },
        "_source": {
          "excludes": []
        },
        "version": true,
        "stored_fields": [
          "*"
        ],
        "script_fields": {},
        "docvalue_fields": [
          "@timestamp",
          "ceph.monitor_health.last_updated",
          "docker.container.created",
          "docker.healthcheck.event.end_date",
          "docker.healthcheck.event.start_date",
          "docker.image.created",
          "kubernetes.container.start_time",
          "kubernetes.event.metadata.timestamp.created",
          "kubernetes.node.start_time",
          "kubernetes.pod.start_time",
          "kubernetes.system.start_time",
          "mongodb.status.background_flushing.last_finished",
          "mongodb.status.local_time",
          "php_fpm.pool.start_time",
          "postgresql.activity.backend_start",
          "postgresql.activity.query_start",
          "postgresql.activity.state_change",
          "postgresql.activity.transaction_start",
          "postgresql.bgwriter.stats_reset",
          "postgresql.database.stats_reset",
          "system.process.cpu.start_time"
        ],
        "query": {
          "bool": {
            "must": [
              {
                "match_all": {}
              },
              {
                "query_string": {
                  "query": "metricset.module:docker",
                  "analyze_wildcard": true,
                  "default_field": "*"
                }
              },
              {
                "query_string": {
                  "analyze_wildcard": true,
                  "default_field": "*",
                  "query": "*"
                }
              },
              {
                "range": {
                  "@timestamp": {
                    "gte": 1525677668153,
                    "lte": 1528269668153,
                    "format": "epoch_millis"
                  }
                }
              }
            ],
            "filter": [],
            "should": [],
            "must_not": []
          }
        }
      },
    containerInfo: {
        "size": 0,
        "aggs": {
          "2": {
            "terms": {
              "field": "docker.container.name",
              "size": 5,
              "order": {
                "1": "desc"
              }
            },
            "aggs": {
              "1": {
                "cardinality": {
                  "field": "docker.container.id"
                }
              },
              "3": {
                "max": {
                  "field": "docker.cpu.total.pct"
                }
              },
              "4": {
                "max": {
                  "field": "docker.diskio.total"
                }
              },
              "5": {
                "max": {
                  "field": "docker.memory.usage.pct"
                }
              },
              "6": {
                "max": {
                  "field": "docker.memory.rss.total"
                }
              }
            }
          }
        },
        "highlight": {
          "pre_tags": [
            "@kibana-highlighted-field@"
          ],
          "post_tags": [
            "@/kibana-highlighted-field@"
          ],
          "fields": {
            "*": {}
          },
          "fragment_size": 2147483647
        },
        "_source": {
          "excludes": []
        },
        "version": true,
        "stored_fields": [
          "*"
        ],
        "script_fields": {},
        "docvalue_fields": [
          "@timestamp",
          "ceph.monitor_health.last_updated",
          "docker.container.created",
          "docker.healthcheck.event.end_date",
          "docker.healthcheck.event.start_date",
          "docker.image.created",
          "kubernetes.container.start_time",
          "kubernetes.event.metadata.timestamp.created",
          "kubernetes.node.start_time",
          "kubernetes.pod.start_time",
          "kubernetes.system.start_time",
          "mongodb.status.background_flushing.last_finished",
          "mongodb.status.local_time",
          "php_fpm.pool.start_time",
          "postgresql.activity.backend_start",
          "postgresql.activity.query_start",
          "postgresql.activity.state_change",
          "postgresql.activity.transaction_start",
          "postgresql.bgwriter.stats_reset",
          "postgresql.database.stats_reset",
          "system.process.cpu.start_time"
        ],
        "query": {
          "bool": {
            "must": [
              {
                "match_all": {}
              },
              {
                "query_string": {
                  "query": "metricset.module:docker",
                  "analyze_wildcard": true,
                  "default_field": "*"
                }
              },
              {
                "query_string": {
                  "analyze_wildcard": true,
                  "default_field": "*",
                  "query": "*"
                }
              },
              {
                "range": {
                  "@timestamp": {
                    "gte": 1525517862679,
                    "lte": 1528109862679,
                    "format": "epoch_millis"
                  }
                }
              }
            ],
            "filter": [],
            "should": [],
            "must_not": []
          }
        }
      },
    memory: {
        "highlight": {
          "pre_tags": [
            "@kibana-highlighted-field@"
          ],
          "post_tags": [
            "@/kibana-highlighted-field@"
          ],
          "fields": {
            "*": {}
          },
          "fragment_size": 2147483647
        },
        "size": 0,
        "_source": {
          "excludes": []
        },
        "aggs": {
          "2": {
            "date_histogram": {
              "field": "@timestamp",
              "interval": "12h",
              "time_zone": "Asia/Kolkata",
              "min_doc_count": 1
            },
            "aggs": {
              "3": {
                "terms": {
                  "field": "docker.container.name",
                  "size": 5,
                  "order": {
                    "1": "desc"
                  }
                },
                "aggs": {
                  "1": {
                    "max": {
                      "field": "docker.memory.usage.total"
                    }
                  }
                }
              }
            }
          }
        },
        "version": true,
        "stored_fields": [
          "*"
        ],
        "script_fields": {},
        "docvalue_fields": [
          "@timestamp",
          "ceph.monitor_health.last_updated",
          "docker.container.created",
          "docker.healthcheck.event.end_date",
          "docker.healthcheck.event.start_date",
          "docker.image.created",
          "kubernetes.container.start_time",
          "kubernetes.event.metadata.timestamp.created",
          "kubernetes.node.start_time",
          "kubernetes.pod.start_time",
          "kubernetes.system.start_time",
          "mongodb.status.background_flushing.last_finished",
          "mongodb.status.local_time",
          "php_fpm.pool.start_time",
          "postgresql.activity.backend_start",
          "postgresql.activity.query_start",
          "postgresql.activity.state_change",
          "postgresql.activity.transaction_start",
          "postgresql.bgwriter.stats_reset",
          "postgresql.database.stats_reset",
          "system.process.cpu.start_time"
        ],
        "query": {
          "bool": {
            "must": [
              {
                "query_string": {
                  "query": "metricset.module:docker AND metricset.name:memory",
                  "analyze_wildcard": true,
                  "default_field": "*"
                }
              },
              {
                "query_string": {
                  "analyze_wildcard": true,
                  "default_field": "*",
                  "query": "*"
                }
              },
              {
                "range": {
                  "@timestamp": {
                    "gte": 1525677809007,
                    "lte": 1528269809007,
                    "format": "epoch_millis"
                  }
                }
              }
            ],
            "filter": [],
            "should": [],
            "must_not": []
          }
        }
      }
}