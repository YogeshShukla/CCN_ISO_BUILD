module.exports = {
    fileActions: {
        "size": 0,
        "_source": {
            "excludes": []
        },
        "aggs": {
            "2": {
                "terms": {
                    "field": "event.action",
                    "size": 5,
                    "order": {
                        "_count": "desc"
                    }
                }
            }
        },
        "version": true,
        "stored_fields": [
            "*"
        ],
        "script_fields": {},
        "docvalue_fields": [
            "@timestamp",
            "file.ctime",
            "file.mtime"
        ],
        "query": {
            "bool": {
                "must": [
                    {
                        "query_string": {
                            "query": "*",
                            "analyze_wildcard": true,
                            "default_field": "*"
                        }
                    },
                    {
                        "match_all": {}
                    },
                    {
                        "query_string": {
                            "analyze_wildcard": true,
                            "default_field": "*",
                            "query": "*"
                        }
                    },
                    {
                        "match_phrase": {
                            "event.module": {
                                "query": "file_integrity"
                            }
                        }
                    },
                    {
                        "range": {
                            "@timestamp": {
                                "gte": 1524212957302,
                                "lte": 1529396957302,
                                "format": "epoch_millis"
                            }
                        }
                    }
                ],
                "filter": [],
                "should": [],
                "must_not": []
            }
        },
        "highlight": {
            "pre_tags": [
                "@kibana-highlighted-field@"
            ],
            "post_tags": [
                "@/kibana-highlighted-field@"
            ],
            "fields": {
                "*": {}
            },
            "fragment_size": 2147483647
        }
    },
    topCreated: {
        "size": 0,
        "_source": {
            "excludes": []
        },
        "aggs": {
            "2": {
                "terms": {
                    "field": "file.path.raw",
                    "size": 10,
                    "order": {
                        "_count": "desc"
                    }
                }
            }
        },
        "version": true,
        "stored_fields": [
            "*"
        ],
        "script_fields": {},
        "docvalue_fields": [
            "@timestamp",
            "file.ctime",
            "file.mtime"
        ],
        "query": {
            "bool": {
                "must": [
                    {
                        "query_string": {
                            "query": "event.action:created",
                            "analyze_wildcard": true,
                            "default_field": "*"
                        }
                    },
                    {
                        "match_all": {}
                    },
                    {
                        "query_string": {
                            "analyze_wildcard": true,
                            "default_field": "*",
                            "query": "*"
                        }
                    },
                    {
                        "match_phrase": {
                            "event.module": {
                                "query": "file_integrity"
                            }
                        }
                    },
                    {
                        "range": {
                            "@timestamp": {
                                "gte": 1524212992301,
                                "lte": 1529396992301,
                                "format": "epoch_millis"
                            }
                        }
                    }
                ],
                "filter": [],
                "should": [],
                "must_not": []
            }
        },
        "highlight": {
            "pre_tags": [
                "@kibana-highlighted-field@"
            ],
            "post_tags": [
                "@/kibana-highlighted-field@"
            ],
            "fields": {
                "*": {}
            },
            "fragment_size": 2147483647
        }
    },
    topDeleted: {
        "size": 0,
        "_source": {
            "excludes": []
        },
        "aggs": {
            "2": {
                "terms": {
                    "field": "file.path.raw",
                    "size": 10,
                    "order": {
                        "_count": "desc"
                    }
                }
            }
        },
        "version": true,
        "stored_fields": [
            "*"
        ],
        "script_fields": {},
        "docvalue_fields": [
            "@timestamp",
            "file.ctime",
            "file.mtime"
        ],
        "query": {
            "bool": {
                "must": [
                    {
                        "query_string": {
                            "analyze_wildcard": true,
                            "query": "event.action:deleted",
                            "default_field": "*"
                        }
                    },
                    {
                        "match_all": {}
                    },
                    {
                        "query_string": {
                            "analyze_wildcard": true,
                            "default_field": "*",
                            "query": "*"
                        }
                    },
                    {
                        "match_phrase": {
                            "event.module": {
                                "query": "file_integrity"
                            }
                        }
                    },
                    {
                        "range": {
                            "@timestamp": {
                                "gte": 1524212992301,
                                "lte": 1529396992301,
                                "format": "epoch_millis"
                            }
                        }
                    }
                ],
                "filter": [],
                "should": [],
                "must_not": []
            }
        },
        "highlight": {
            "pre_tags": [
                "@kibana-highlighted-field@"
            ],
            "post_tags": [
                "@/kibana-highlighted-field@"
            ],
            "fields": {
                "*": {}
            },
            "fragment_size": 2147483647
        }
    },
    topUpdated: {
        "size": 0,
        "_source": {
            "excludes": []
        },
        "aggs": {
            "3": {
                "terms": {
                    "field": "file.path.raw",
                    "size": 10,
                    "order": {
                        "_count": "desc"
                    }
                }
            }
        },
        "version": true,
        "stored_fields": [
            "*"
        ],
        "script_fields": {},
        "docvalue_fields": [
            "@timestamp",
            "file.ctime",
            "file.mtime"
        ],
        "query": {
            "bool": {
                "must": [
                    {
                        "query_string": {
                            "query": "event.action:updated OR event.action:attributes_modified",
                            "analyze_wildcard": true,
                            "default_field": "*"
                        }
                    },
                    {
                        "match_all": {}
                    },
                    {
                        "query_string": {
                            "analyze_wildcard": true,
                            "default_field": "*",
                            "query": "*"
                        }
                    },
                    {
                        "match_phrase": {
                            "event.module": {
                                "query": "file_integrity"
                            }
                        }
                    },
                    {
                        "range": {
                            "@timestamp": {
                                "gte": 1524213025034,
                                "lte": 1529397025034,
                                "format": "epoch_millis"
                            }
                        }
                    }
                ],
                "filter": [],
                "should": [],
                "must_not": []
            }
        },
        "highlight": {
            "pre_tags": [
                "@kibana-highlighted-field@"
            ],
            "post_tags": [
                "@/kibana-highlighted-field@"
            ],
            "fields": {
                "*": {}
            },
            "fragment_size": 2147483647
        }
    }
}