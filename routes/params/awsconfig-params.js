module.exports = {
  configImageId: {
    "query": {
      "bool": {
        "must": [
          {
            "match_all": {}
          },
          {
            "match_all": {}
          },
          {
            "range": {
              "snapshotTimeIso": {
                "gte": 1528867446867,
                "lte": 1528871046868,
                "format": "epoch_millis"
              }
            }
          }
        ],
        "must_not": []
      }
    },
    "size": 0,
    "_source": {
      "excludes": []
    },
    "aggs": {
      "2": {
        "terms": {
          "field": "configuration.imageId.raw",
          "size": 20,
          "order": {
            "_count": "desc"
          }
        }
      }
    },
    "version": true,
    "highlight": {
      "pre_tags": [
        "@kibana-highlighted-field@"
      ],
      "post_tags": [
        "@/kibana-highlighted-field@"
      ],
      "fields": {
        "*": {
          "highlight_query": {
            "bool": {
              "must": [
                {
                  "match_all": {}
                },
                {
                  "match_all": {}
                },
                {
                  "range": {
                    "snapshotTimeIso": {
                      "gte": 1528867446867,
                      "lte": 1528871046868,
                      "format": "epoch_millis"
                    }
                  }
                }
              ],
              "must_not": []
            }
          }
        }
      },
      "fragment_size": 2147483647
    }
  },
  deletedOnTermination: {
    "query": {
      "bool": {
        "must": [
          {
            "match_all": {}
          },
          {
            "match_all": {}
          },
          {
            "range": {
              "snapshotTimeIso": {
                "gte": 1528867446868,
                "lte": 1528871046868,
                "format": "epoch_millis"
              }
            }
          }
        ],
        "must_not": []
      }
    },
    "size": 0,
    "_source": {
      "excludes": []
    },
    "aggs": {
      "2": {
        "terms": {
          "field": "configuration.attachment.deleteOnTermination",
          "size": 5,
          "order": {
            "_count": "desc"
          }
        }
      }
    },
    "version": true,
    "highlight": {
      "pre_tags": [
        "@kibana-highlighted-field@"
      ],
      "post_tags": [
        "@/kibana-highlighted-field@"
      ],
      "fields": {
        "*": {
          "highlight_query": {
            "bool": {
              "must": [
                {
                  "match_all": {}
                },
                {
                  "match_all": {}
                },
                {
                  "range": {
                    "snapshotTimeIso": {
                      "gte": 1528867446868,
                      "lte": 1528871046868,
                      "format": "epoch_millis"
                    }
                  }
                }
              ],
              "must_not": []
            }
          }
        }
      },
      "fragment_size": 2147483647
    }
  },
  resourceType: {
    "query": {
      "bool": {
        "must": [
          {
            "match_all": {}
          },
          {
            "match_all": {}
          },
          {
            "range": {
              "snapshotTimeIso": {
                "gte": 1528867446869,
                "lte": 1528871046869,
                "format": "epoch_millis"
              }
            }
          }
        ],
        "must_not": []
      }
    },
    "size": 0,
    "_source": {
      "excludes": []
    },
    "aggs": {
      "2": {
        "terms": {
          "field": "relationships.resourceType.raw",
          "size": 20,
          "order": {
            "_count": "desc"
          }
        }
      }
    },
    "version": true,
    "highlight": {
      "pre_tags": [
        "@kibana-highlighted-field@"
      ],
      "post_tags": [
        "@/kibana-highlighted-field@"
      ],
      "fields": {
        "*": {
          "highlight_query": {
            "bool": {
              "must": [
                {
                  "match_all": {}
                },
                {
                  "match_all": {}
                },
                {
                  "range": {
                    "snapshotTimeIso": {
                      "gte": 1528867446869,
                      "lte": 1528871046869,
                      "format": "epoch_millis"
                    }
                  }
                }
              ],
              "must_not": []
            }
          }
        }
      },
      "fragment_size": 2147483647
    }
  },
  resourceCreation: {
    "query": {
      "bool": {
        "must": [
          {
            "match_all": {}
          },
          {
            "match_all": {}
          },
          {
            "range": {
              "snapshotTimeIso": {
                "gte": 1528867446869,
                "lte": 1528871046869,
                "format": "epoch_millis"
              }
            }
          }
        ],
        "must_not": []
      }
    },
    "size": 0,
    "_source": {
      "excludes": []
    },
    "aggs": {
      "2": {
        "date_histogram": {
          "field": "resourceCreationTime",
          "interval": "1w",
          "time_zone": "Asia/Kolkata",
          "min_doc_count": 1
        }
      }
    },
    "version": true,
    "highlight": {
      "pre_tags": [
        "@kibana-highlighted-field@"
      ],
      "post_tags": [
        "@/kibana-highlighted-field@"
      ],
      "fields": {
        "*": {
          "highlight_query": {
            "bool": {
              "must": [
                {
                  "match_all": {}
                },
                {
                  "match_all": {}
                },
                {
                  "range": {
                    "snapshotTimeIso": {
                      "gte": 1528867446869,
                      "lte": 1528871046869,
                      "format": "epoch_millis"
                    }
                  }
                }
              ],
              "must_not": []
            }
          }
        }
      },
      "fragment_size": 2147483647
    }
  }
}