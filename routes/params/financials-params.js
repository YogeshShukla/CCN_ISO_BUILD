module.exports = {
    s3reqCost: {
      "query": {
        "bool": {
          "must": [
            {
              "query_string": {
                "query": "*",
                "analyze_wildcard": true
              }
            },
            {
              "query_string": {
                "analyze_wildcard": true,
                "query": "*"
              }
            },
            {
              "range": {
                "lineItem/UsageStartDate": {
                  "gte": 1522925015793,
                  "lte": 1528109015794,
                  "format": "epoch_millis"
                }
              }
            }
          ],
          "must_not": []
        }
      },
      "size": 0,
      "_source": {
        "excludes": []
      },
      "aggs": {
        "2": {
          "filters": {
            "filters": {
              "AmazonS3": {
                "query_string": {
                  "query": "AmazonS3",
                  "analyze_wildcard": true
                }
              }
            }
          },
          "aggs": {
            "1": {
              "sum": {
                "field": "lineItem/UnblendedCost"
              }
            },
            "3": {
              "terms": {
                "field": "product/productFamily.keyword",
                "size": 3,
                "order": {
                  "1": "desc"
                }
              },
              "aggs": {
                "1": {
                  "sum": {
                    "field": "lineItem/UnblendedCost"
                  }
                }
              }
            }
          }
        }
      }
    },
    separateServiceCost: {
      "query": {
        "bool": {
          "must": [
            {
              "query_string": {
                "analyze_wildcard": true,
                "query": "*"
              }
            },
            {
              "query_string": {
                "analyze_wildcard": true,
                "query": "*"
              }
            },
            {
              "range": {
                "lineItem/UsageStartDate": {
                  "gte": 1522925015792,
                  "lte": 1528109015792,
                  "format": "epoch_millis"
                }
              }
            }
          ],
          "must_not": []
        }
      },
      "size": 0,
      "_source": {
        "excludes": []
      },
      "aggs": {
        "2": {
          "terms": {
            "field": "lineItem/ProductCode.keyword",
            "size": 8,
            "order": {
              "1": "desc"
            }
          },
          "aggs": {
            "1": {
              "sum": {
                "field": "lineItem/UnblendedCost"
              }
            }
          }
        }
      }
    },
    awsTotalCost: {
      "query": {
        "bool": {
          "must": [
            {
              "query_string": {
                "query": "*",
                "analyze_wildcard": true
              }
            },
            {
              "query_string": {
                "analyze_wildcard": true,
                "query": "*"
              }
            },
            {
              "range": {
                "lineItem/UsageStartDate": {
                  "gte": 1522925015793,
                  "lte": 1528109015793,
                  "format": "epoch_millis"
                }
              }
            }
          ],
          "must_not": []
        }
      },
      "size": 0,
      "_source": {
        "excludes": []
      },
      "aggs": {
        "1": {
          "sum": {
            "field": "lineItem/UnblendedCost"
          }
        }
      }
    },
    ec2instanceCost: {
      "query": {
        "bool": {
          "must": [
            {
              "query_string": {
                "analyze_wildcard": true,
                "query": "*"
              }
            },
            {
              "query_string": {
                "analyze_wildcard": true,
                "query": "*"
              }
            },
            {
              "range": {
                "lineItem/UsageStartDate": {
                  "gte": 1522925015793,
                  "lte": 1528109015793,
                  "format": "epoch_millis"
                }
              }
            }
          ],
          "must_not": []
        }
      },
      "size": 0,
      "_source": {
        "excludes": []
      },
      "aggs": {
        "2": {
          "filters": {
            "filters": {
              "all Amazon EC2 Instances": {
                "query_string": {
                  "analyze_wildcard": true,
                  "query": "*AmazonEC2*"
                }
              }
            }
          },
          "aggs": {
            "1": {
              "sum": {
                "field": "lineItem/UnblendedCost"
              }
            },
            "3": {
              "filters": {
                "filters": {
                  "Spot EC2 instaIces": {
                    "query_string": {
                      "analyze_wildcard": true,
                      "query": "*Spot*"
                    }
                  },
                  "OnDemand EC2 Instances": {
                    "query_string": {
                      "analyze_wildcard": true,
                      "query": "NOT *Spot*"
                    }
                  }
                }
              },
              "aggs": {
                "1": {
                  "sum": {
                    "field": "lineItem/UnblendedCost"
                  }
                }
              }
            }
          }
        }
      }
    }
}