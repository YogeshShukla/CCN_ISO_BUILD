module.exports = {
    availablePatches: {
        "size": 0,
        "_source": {
          "excludes": []
        },
        "aggs": {
          "2": {
            "terms": {
              "field": "mappings._doc.properties.available_count.value.keyword",
              "size": 5,
              "order": {
                "_count": "desc"
              }
            }
          }
        },
        "version": true,
        "stored_fields": [
          "*"
        ],
        "script_fields": {},
        "docvalue_fields": [],
        "query": {
          "bool": {
            "must": [
              {
                "match_all": {}
              },
              {
                "match_all": {}
              }
            ],
            "filter": [],
            "should": [],
            "must_not": []
          }
        },
        "highlight": {
          "pre_tags": [
            "@kibana-highlighted-field@"
          ],
          "post_tags": [
            "@/kibana-highlighted-field@"
          ],
          "fields": {
            "*": {}
          },
          "fragment_size": 2147483647
        }
      },
      
    criticalPatches: {
        "size": 0,
        "_source": {
          "excludes": []
        },
        "aggs": {
          "2": {
            "terms": {
              "field": "mappings._doc.properties.critical.value.keyword",
              "size": 5,
              "order": {
                "_count": "desc"
              }
            }
          }
        },
        "version": true,
        "stored_fields": [
          "*"
        ],
        "script_fields": {},
        "docvalue_fields": [],
        "query": {
          "bool": {
            "must": [
              {
                "match_all": {}
              },
              {
                "match_all": {}
              }
            ],
            "filter": [],
            "should": [],
            "must_not": []
          }
        },
        "highlight": {
          "pre_tags": [
            "@kibana-highlighted-field@"
          ],
          "post_tags": [
            "@/kibana-highlighted-field@"
          ],
          "fields": {
            "*": {}
          },
          "fragment_size": 2147483647
        }
      },
    importantPatches: {
        "size": 0,
        "_source": {
          "excludes": []
        },
        "aggs": {
          "2": {
            "terms": {
              "field": "mappings._doc.properties.important.value.keyword",
              "size": 5,
              "order": {
                "_count": "desc"
              }
            }
          }
        },
        "version": true,
        "stored_fields": [
          "*"
        ],
        "script_fields": {},
        "docvalue_fields": [],
        "query": {
          "bool": {
            "must": [
              {
                "match_all": {}
              },
              {
                "match_all": {}
              }
            ],
            "filter": [],
            "should": [],
            "must_not": []
          }
        },
        "highlight": {
          "pre_tags": [
            "@kibana-highlighted-field@"
          ],
          "post_tags": [
            "@/kibana-highlighted-field@"
          ],
          "fields": {
            "*": {}
          },
          "fragment_size": 2147483647
        }
      },
    moderatePatches: {
        "size": 0,
        "_source": {
          "excludes": []
        },
        "aggs": {
          "2": {
            "terms": {
              "field": "mappings._doc.properties.moderate.value.keyword",
              "size": 5,
              "order": {
                "_count": "desc"
              }
            }
          }
        },
        "version": true,
        "stored_fields": [
          "*"
        ],
        "script_fields": {},
        "docvalue_fields": [],
        "query": {
          "bool": {
            "must": [
              {
                "match_all": {}
              },
              {
                "match_all": {}
              }
            ],
            "filter": [],
            "should": [],
            "must_not": []
          }
        },
        "highlight": {
          "pre_tags": [
            "@kibana-highlighted-field@"
          ],
          "post_tags": [
            "@/kibana-highlighted-field@"
          ],
          "fields": {
            "*": {}
          },
          "fragment_size": 2147483647
        }
      }
      




}
