module.exports = {
    topHostsCpu : {
        "timerange": {
            "timezone": "Asia/Kolkata",
            "min": "2018-06-28T09:11:47.424Z",
            "max": "2018-06-28T09:26:47.424Z"
        },
        "filters": [
            {
                "bool": {
                    "must": [
                        {
                            "query_string": {
                                "analyze_wildcard": true,
                                "default_field": "*",
                                "query": "*"
                            }
                        }
                    ],
                    "must_not": []
                }
            }
        ],
        "panels": [
            {
                "id": "31e5afa0-1b1c-11e7-b09e-037021c4f8df",
                "type": "top_n",
                "series": [
                    {
                        "id": "31e5afa1-1b1c-11e7-b09e-037021c4f8df",
                        "color": "#68BC00",
                        "split_mode": "terms",
                        "metrics": [
                            {
                                "id": "31e5afa2-1b1c-11e7-b09e-037021c4f8df",
                                "type": "avg",
                                "field": "system.cpu.user.pct"
                            }
                        ],
                        "seperate_axis": 0,
                        "axis_position": "right",
                        "formatter": "percent",
                        "chart_type": "line",
                        "line_width": 1,
                        "point_size": 1,
                        "fill": 0.5,
                        "stacked": "none",
                        "terms_field": "beat.name",
                        "terms_order_by": "31e5afa2-1b1c-11e7-b09e-037021c4f8df",
                        "terms_size": "10"
                    }
                ],
                "time_field": "@timestamp",
                "index_pattern": "metricbeat-*",
                "interval": "auto",
                "axis_position": "left",
                "axis_formatter": "number",
                "show_legend": 1,
                "bar_color_rules": [
                    {
                        "value": 0,
                        "id": "33349dd0-1b1c-11e7-b09e-037021c4f8df",
                        "bar_color": "rgba(104,188,0,1)",
                        "opperator": "gte"
                    },
                    {
                        "value": 0.6,
                        "id": "997dc440-1b1c-11e7-b09e-037021c4f8df",
                        "bar_color": "rgba(254,146,0,1)",
                        "opperator": "gte"
                    },
                    {
                        "value": 0.85,
                        "id": "a10d7f20-1b1c-11e7-b09e-037021c4f8df",
                        "bar_color": "rgba(211,49,21,1)",
                        "opperator": "gte"
                    }
                ],
                "drilldown_url": "../app/kibana#/dashboard/79ffd6e0-faa0-11e6-947f-177f697178b8?_a=(query:(query_string:(analyze_wildcard:!t,query:'beat.name:\"{{key}}\"')))",
                "filter": "",
                "show_grid": 1
            }
        ],
        "state": {}
    },
    topHostsMemory : {
        "timerange": {
            "timezone": "Asia/Kolkata",
            "min": "2018-06-28T09:11:47.424Z",
            "max": "2018-06-28T09:26:47.424Z"
        },
        "filters": [
            {
                "bool": {
                    "must": [
                        {
                            "query_string": {
                                "analyze_wildcard": true,
                                "default_field": "*",
                                "query": "*"
                            }
                        }
                    ],
                    "must_not": []
                }
            }
        ],
        "panels": [
            {
                "id": "31e5afa0-1b1c-11e7-b09e-037021c4f8df",
                "type": "top_n",
                "series": [
                    {
                        "id": "31e5afa1-1b1c-11e7-b09e-037021c4f8df",
                        "color": "#68BC00",
                        "split_mode": "terms",
                        "metrics": [
                            {
                                "id": "31e5afa2-1b1c-11e7-b09e-037021c4f8df",
                                "type": "avg",
                                "field": "system.memory.actual.used.pct"
                            }
                        ],
                        "seperate_axis": 0,
                        "axis_position": "right",
                        "formatter": "percent",
                        "chart_type": "line",
                        "line_width": 1,
                        "point_size": 1,
                        "fill": 0.5,
                        "stacked": "none",
                        "terms_field": "beat.name",
                        "terms_order_by": "31e5afa2-1b1c-11e7-b09e-037021c4f8df",
                        "terms_size": "10"
                    }
                ],
                "time_field": "@timestamp",
                "index_pattern": "metricbeat-*",
                "interval": "auto",
                "axis_position": "left",
                "axis_formatter": "number",
                "show_legend": 1,
                "bar_color_rules": [
                    {
                        "value": 0,
                        "id": "33349dd0-1b1c-11e7-b09e-037021c4f8df",
                        "bar_color": "rgba(104,188,0,1)",
                        "opperator": "gte"
                    },
                    {
                        "value": 0.6,
                        "id": "997dc440-1b1c-11e7-b09e-037021c4f8df",
                        "bar_color": "rgba(254,146,0,1)",
                        "opperator": "gte"
                    },
                    {
                        "value": 0.85,
                        "id": "a10d7f20-1b1c-11e7-b09e-037021c4f8df",
                        "bar_color": "rgba(211,49,21,1)",
                        "opperator": "gte"
                    }
                ],
                "drilldown_url": "../app/kibana#/dashboard/79ffd6e0-faa0-11e6-947f-177f697178b8?_a=(query:(query_string:(analyze_wildcard:!t,query:'beat.name:\"{{key}}\"')))",
                "filter": "",
                "show_grid": 1
            }
        ],
        "state": {}
    },

    topProcessCpu : {
        "timerange": {
            "timezone": "Asia/Kolkata",
            "min": "2018-06-25T09:50:00.743Z",
            "max": "2018-06-28T09:50:00.743Z"
        },
        "filters": [
            {
                "bool": {
                    "must": [
                        {
                            "match_all": {}
                        }
                    ],
                    "must_not": []
                }
            }
        ],
        "panels": [
            {
                "id": "5f5b8d50-1b18-11e7-b09e-037021c4f8df",
                "type": "top_n",
                "series": [
                    {
                        "id": "5f5b8d51-1b18-11e7-b09e-037021c4f8df",
                        "color": "#68BC00",
                        "split_mode": "terms",
                        "metrics": [
                            {
                                "id": "5f5b8d52-1b18-11e7-b09e-037021c4f8df",
                                "type": "avg",
                                "field": "system.process.cpu.total.pct"
                            }
                        ],
                        "seperate_axis": 0,
                        "axis_position": "right",
                        "formatter": "percent",
                        "chart_type": "line",
                        "line_width": 1,
                        "point_size": 1,
                        "fill": 0.5,
                        "stacked": "none",
                        "terms_field": "system.process.name",
                        "terms_order_by": "5f5b8d52-1b18-11e7-b09e-037021c4f8df"
                    }
                ],
                "time_field": "@timestamp",
                "index_pattern": "metricbeat-*",
                "interval": "auto",
                "axis_position": "left",
                "axis_formatter": "number",
                "show_legend": 1,
                "bar_color_rules": [
                    {
                        "value": 0,
                        "id": "60e11be0-1b18-11e7-b09e-037021c4f8df",
                        "bar_color": "rgba(104,188,0,1)",
                        "opperator": "gte"
                    }
                ],
                "drilldown_url": "",
                "filter": "",
                "show_grid": 1
            }
        ],
        "state": {}
    },
    topProcessMemory : {
        "timerange": {
            "timezone": "Asia/Kolkata",
            "min": "2018-06-25T09:44:21.138Z",
            "max": "2018-06-28T09:44:21.138Z"
        },
        "filters": [
            {
                "bool": {
                    "must": [
                        {
                            "match_all": {}
                        }
                    ],
                    "must_not": []
                }
            }
        ],
        "panels": [
            {
                "id": "edfceb30-1b18-11e7-b09e-037021c4f8df",
                "type": "top_n",
                "series": [
                    {
                        "id": "edfceb31-1b18-11e7-b09e-037021c4f8df",
                        "color": "#68BC00",
                        "split_mode": "terms",
                        "metrics": [
                            {
                                "id": "edfceb32-1b18-11e7-b09e-037021c4f8df",
                                "type": "avg",
                                "field": "system.process.memory.rss.pct"
                            }
                        ],
                        "seperate_axis": 0,
                        "axis_position": "right",
                        "formatter": "percent",
                        "chart_type": "line",
                        "line_width": 1,
                        "point_size": 1,
                        "fill": 0.5,
                        "stacked": "none",
                        "terms_field": "system.process.name",
                        "terms_order_by": "edfceb32-1b18-11e7-b09e-037021c4f8df"
                    }
                ],
                "time_field": "@timestamp",
                "index_pattern": "metricbeat-*",
                "interval": "auto",
                "axis_position": "left",
                "axis_formatter": "number",
                "show_legend": 1,
                "bar_color_rules": [
                    {
                        "value": 0,
                        "id": "efb9b660-1b18-11e7-b09e-037021c4f8df",
                        "bar_color": "rgba(104,188,0,1)",
                        "opperator": "gte"
                    },
                    {
                        "value": 0.7,
                        "id": "17fcb820-1b19-11e7-b09e-037021c4f8df",
                        "bar_color": "rgba(254,146,0,1)",
                        "opperator": "gte"
                    },
                    {
                        "value": 0.85,
                        "id": "1dd61070-1b19-11e7-b09e-037021c4f8df",
                        "bar_color": "rgba(211,49,21,1)",
                        "opperator": "gte"
                    }
                ],
                "drilldown_url": "",
                "filter": "",
                "show_grid": 1
            }
        ],
        "state": {}
    }
}