const router = require('express').Router();
//const elasticSearch = require('elasticsearch');

const chartApi = require('./charts-api');
const trending = require('./trending-api');
const ospatches = require('./ospatches-api');

 router.use('/trending',trending);
 
 router.use('/ospatches',ospatches);
 router.use('/:pathname', chartApi);

module.exports = router;