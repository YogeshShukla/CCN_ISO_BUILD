//const http = require('http');
const request = require('request');
const router = require('express').Router();
const params = require('./params/trending-params');
let stringifyData = '';
//const trendingUri = 'http://54.84.12.123:5601/api/metrics/vis/data';
//const trendingUri = 'http://54.175.196.189:5601/api/metrics/vis/data';
const trendingUri = 'http://34.200.246.73:5601/api/metrics/vis/data';


const trendingHeaders = {
    'Content-Type': 'application/json',
    'kbn-xsrf': 'reporting'
};
router.post('/', prepareSearchObj, (req, res) => {
    request({
        headers: trendingHeaders,
        uri: trendingUri,
        body: stringifyData,
        method: 'POST'
    }, (err, httpres, body) => {
        res.json(httpres);
    });
});


function prepareSearchObj(req, res, next) {
    let trendingParams = params[req.body.target];

    var minTime = new Date();
    minTime.setMinutes( minTime.getMinutes() - 15 );

   trendingParams.timerange.min = (minTime).toISOString();
   trendingParams.timerange.max = new Date().toISOString();


    stringifyData = JSON.stringify(trendingParams);
    return next();
}

module.exports = router;
