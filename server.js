const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
var path = require('path');

const app = express();
const router = require('./routes/router');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(cors());

app.use(express.static(path.join(__dirname, 'dist')));

app.use('/api/v1', router);

app.listen(8080, '0.0.0.0', ()=>{ console.info('Server Started at 8080') })

