import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { InfrastructureComponent } from './dashboard/infrastructure/infrastructure.component';
import { ApplicationsComponent } from './dashboard/applications/applications.component';
import { TrendingComponent } from './dashboard/trending/trending.component';
import { FinancialsComponent } from './dashboard/financials/financials.component';
import { ConfigComponent } from './dashboard/config/config.component';
import { AuditComponent } from './dashboard/audit/audit.component';
import { osPatchesComponent } from './dashboard/osPatches/osPatches.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent ,
 
children: [

    { path: 'infrastructure', component: InfrastructureComponent },
    { path: 'applications', component: ApplicationsComponent },
    { path: 'trending', component: TrendingComponent },
    { path: 'financials', component: FinancialsComponent },
    { path: 'config', component: ConfigComponent },
     { path: 'audit', component: AuditComponent },
    { path: 'osPatches', component: osPatchesComponent },
    
  ]},
  { path: 'login', component: LoginComponent 
}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
