import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { AppRoutingModule } from './app-routing.module';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { AppComponent } from './app.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import { HeaderNavComponent } from './header-nav/header-nav.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PieChartComponent } from './charts/pie-chart/pie-chart.component';
import { BarChartComponent } from './charts/bar-chart/bar-chart.component';
import { MaterialModule } from './material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LineChartComponent } from './charts/line-chart/line-chart.component';
import { GuagechartComponent } from './charts/guagechart/guagechart.component';
import { PatternDetectionDetailsComponent } from './charts/pattern-detection-details/pattern-detection-details.component';

import { ChartsService } from '../app/charts/charts.service';
import { InfrastructureService } from '../app/services/infrastructure.service';
import { ApplicationService } from '../app/services/applications.service';
import { FinancialsService } from '../app/services/financials.service';
import { AwsconfigService } from '../app/services/awsconfig.service';
import { SharedService } from '../app/services/shared.service';
import { AuditService } from '../app/services/audit.service';
import { osPatchesService } from '../app/services/osPatches.service';
import { LoginComponent } from './login/login.component';


import { TrendingService } from '../app/services/trending.service';
import { InfrastructureComponent } from './dashboard/infrastructure/infrastructure.component';
import { ApplicationsComponent } from './dashboard/applications/applications.component';
import { TrendingComponent } from './dashboard/trending/trending.component';
import { FinancialsComponent } from './dashboard/financials/financials.component';
import { ConfigComponent } from './dashboard/config/config.component';
import { AuditComponent } from './dashboard/audit/audit.component';
import { osPatchesComponent } from './dashboard/osPatches/osPatches.component';
import { osPatchesAccordionComponent } from './dashboard/osPatches/patches-accordion/patches-accordion.component';


@NgModule({
  declarations: [
    AppComponent,
    SideNavComponent,
    HeaderNavComponent,
    DashboardComponent,
    PieChartComponent,
    BarChartComponent,
    LineChartComponent,
    GuagechartComponent,
    PatternDetectionDetailsComponent,
    InfrastructureComponent,
    ApplicationsComponent,
    TrendingComponent,
    FinancialsComponent,
    ConfigComponent,
    AuditComponent,
    osPatchesComponent,
    LoginComponent,
    osPatchesAccordionComponent

  ],
  imports: [
    BrowserModule,
    HttpModule,
    Ng2GoogleChartsModule,
    AppRoutingModule,
    MaterialModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    FormsModule,
    NgbModule.forRoot()
  ],
  providers: [
    ChartsService,
    InfrastructureService,
    ApplicationService,
    FinancialsService,
    AwsconfigService,
    AuditService,
    osPatchesService,
    TrendingService,
    SharedService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
