import { Component, OnInit , Input , Output, EventEmitter} from '@angular/core';
import { ChartSelectEvent } from 'ng2-google-charts';
import { ChartReadyEvent } from 'ng2-google-charts';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss']
})
export class BarChartComponent implements OnInit {
  public selectEvent: ChartSelectEvent;
  showLoader = true;
  isError = false;
  @Input () columnChartData;
  @Input () type;
  @Output() selectedBar = new EventEmitter<number>();
  constructor() { }

  ngOnInit() {
  }

  public select(event: ChartSelectEvent) {
    this.selectEvent = event;
    if(this.type=='patterndetection'){
      this.selectedBar.emit(this.selectEvent.row);
    }
    
  }

  ready(eve){
    this.showLoader = false;

  }

  error(eve){
    this.showLoader =false; 
    this.isError = true;
  }
}
