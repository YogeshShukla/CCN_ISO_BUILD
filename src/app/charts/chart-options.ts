export const chartOptions = {

    knowledgeBase: {
        pieHole: 0.45,
        chartArea: { width: '90%', height: '85%' },
        backgroundColor: '#FFFFFF',
        colors: ['#ffb600', '#eb8c00', '#dc6900', '#a32020', '#602320'],
        legend: {
            alignment: 'center',
            textStyle: { color: 'white', fontSize: 14 }
        },
        tooltip: { ignoreBounds: true },
        pieSliceBorderColor: 'none'
    },
    pdr: {
        chartArea: { width: '85%', height: '70%' },
        legend: {
            textStyle: { color: 'white' },
            position: 'bottom',
            alignment: 'center'
        },
        bar: {
            groupWidth: 30
        },
        hAxis: {
            textStyle: { color: 'white' }
        },
        vAxis: {
            textStyle: { color: 'white' }
        },
        backgroundColor: '#2d3035',
        colors: ['#ffb900']
    },
    gauge: {
        animation: { easing: 'out' },
        greenFrom: 1, greenTo: 4,
        minorTicks: 10,
        min: 0,
        max: 100,
        majorTicks: 0,
        greenColor: '#d0e9c6'
    },
    memory: {
        series: {
            pointsVisible: true
        },
        legend: {
            position: 'bottom',
            alignment: 'center',
            textStyle: { color: 'white' }
        },
        hAxis: {
            textStyle: { color: 'white' }
        },
        vAxis: {
            textStyle: { color: 'white' }
        },
        chartArea: { width: '85%', height: '70%' },
        backgroundColor: '#2d3035',
        colors: ['#ffc82d', '#a32020', '#dc6900', '#602320', '#ffb600']
    },
    cpu: {
        chartArea: { width: '85%', height: '70%' },
        legend: {
            textStyle: { color: 'white' },
            position: 'bottom',
            alignment: 'center'
        },
        bar: {
            groupWidth: 30
        },
        hAxis: {
            textStyle: { color: 'white' }
        },
        vAxis: {
            textStyle: { color: 'white' }
        },
        backgroundColor: '#FFFFFF',
        colors: ['#ffb900']
    },
    storage: {
        chartArea: { width: '85%', height: '70%' },
        legend: {
            textStyle: { color: 'white' },
            position: 'bottom',
            alignment: 'center'
        },
        bar: {
            groupWidth: 30
        },
        hAxis: {
            textStyle: { color: 'white' }
        },
        vAxis: {
            textStyle: { color: 'white' }
        },
        backgroundColor: '#2d3035',
        colors: ['#ffb900']
    },
    network: {
        pieHole: 0.45,
        chartArea: { width: '85%', height: '70%' },
        backgroundColor: '#2d3035',
        colors: ['#ffb600', '#eb8c00', '#dc6900', '#a32020', '#602320'],
        legend: {
            alignment: 'center',
            textStyle: { color: 'white', fontSize: 14 }
        },
        pieSliceText: 'none',
        tooltip: { ignoreBounds: false },
        pieSliceBorderColor: 'none'
    }

}