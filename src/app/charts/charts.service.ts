import { Component, Input, Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
//import 'rxjs/add/operator/catch';

@Injectable()
export class ChartsService {

  constructor(private http: Http) {}

  public convertPieChartData(resp, type) {
    let Kb_array = [];
    Kb_array.push(['Title', type]);
    let res = type == 'knowledgebase' ? resp.accuracyLevels : resp.networks;
    let k = 0;
    res.forEach(item => {
      for (var key in item) {
        if (item.hasOwnProperty(key)) {
          Kb_array.push([key, item[key]])
        }
        k++;
      }
    })

    return (Kb_array)
  }
  public knowledgeBase() {
    return this.http.get('assets/dataList/accuracy_graph.json')
      .map(res => this.convertPieChartData(res.json(), 'knowledgebase')).toPromise();
  }

  public network() {
    return this.http.get('assets/dataList/networks.json')
      .map(res => this.convertPieChartData(res.json(), 'network')).toPromise();
  }

  public pdr() {
    return this.http.get('assets/dataList/pdr.json')
      .map(res => this.convertPdrData(res.json())).toPromise();
  }

  public pdrData() {
    return this.http.get('assets/dataList/pdr.json')
      .map(res => res.json()).toPromise();

  }

  convertPdrData(resp) {
    let barChart_array = [];
    barChart_array.push(['Title', 'User'])
    let res = resp.xAxis;
    res.forEach(item => {
      barChart_array.push([item.date, item.patternsDetected])
    })
    return (barChart_array)
  }


  public gauge() {
    return this.http.get('assets/dataList/speed_gauge.json')
      .map(res => this.convertGaugeData(res.json())).toPromise();
  }

  convertGaugeData(resp) {
    return resp;
  }

  public memory() {
    return this.http.get('assets/dataList/Memory_infra.json')
      .map(res => this.convertMemoryData(res.json())).toPromise();
  }

  convertMemoryData(resp) {

    let memory_array = [];

    let dates = ['Dates'];
    dates = dates.concat(resp.types);
    memory_array.push(dates)

    resp.xAxis.forEach(item => {
      let temp_array = [];
      temp_array.push(item.date);
      temp_array = temp_array.concat(item.units)
      memory_array.push(temp_array)
    })
    return memory_array;
  }

  public cpu() {
    return this.http.get('assets/dataList/CPU_infra.json')
      .map(res => this.convertCpuData(res.json())).toPromise();
  }

  convertCpuData(resp) {
    let cpuData = [];
    let titles = ['Title'];
    titles = titles.concat(resp.types);
    cpuData.push(titles);

    resp.xAxis.forEach(item => {
      let temp_array = [item.date, item.unit];
      cpuData.push(temp_array);
    })
    return cpuData;
  }

  public storage() {
    return this.http.get('assets/dataList/Storage_infra.json')
      .map(res => this.convertStorageData(res.json())).toPromise();
  }

  convertStorageData(resp) {
    return this.convertCpuData(resp)
  }

}