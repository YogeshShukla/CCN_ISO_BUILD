import { Component, OnInit, Input } from '@angular/core';
import { ChartReadyEvent } from 'ng2-google-charts';
@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss']
})
export class PieChartComponent implements OnInit {
  @Input () pieChartData;
  @Input() totalValue;
  @Input() className = ""
  totalvalue = '';
  isChartLoaded = false;
  isError = false;
  constructor() { }

  ngOnInit() {
  }

  error(event){
    this.isError = true;
    this.isChartLoaded = true
    
  }

  ready(eve){
    //console.log(eve);
    this.totalvalue = this.totalValue;
    this.isChartLoaded = true;
  }

}
