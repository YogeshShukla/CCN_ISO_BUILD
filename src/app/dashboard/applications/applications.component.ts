import { Component, OnInit } from '@angular/core';
import { ChartsService } from '../../charts/charts.service';
import { ApplicationService } from '../../services/applications.service';
import { chartOptions } from '../../charts/chart-options';
import { applicationChartOptions } from '../../options/application-chart-options';
import { SharedService } from '../../services/shared.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-applications',
  templateUrl: './applications.component.html',
  styleUrls: ['./applications.component.scss']
})
export class ApplicationsComponent implements OnInit {

  storageChartData = {};
  memoryChartData = {};
  knowledgeBaseData = {};
  knowledgeBaseLoaded = false;
  selectedResource: string = 'Resource';
  selectedResourceValue: string = '';
  expandedChart: string = '';
  cpuUsageData = {};
  containerCountData = {};
  containerInfoData = {};
  memoryData = {};
  expandedCpuUsageData={};
  expandedContainerCountData={};
  expandedMemoryData={};
  chartList = { cpu: "Docker Containers CPU Usage", containerCount: "Docker Containers by Service", containerInfo: "Docker Image Sizes", memory: "Docker Containers Memory Consumption" }

  constructor(private sharedService: SharedService, private chartsService: ChartsService, private applicationService: ApplicationService, private modalService: NgbModal) { }

  resources = [
    { value: 'resource1', viewValue: 'Resource 1' },
    { value: 'resource2', viewValue: 'Resource 2' },
    { value: 'resource3', viewValue: 'Resource 3' },
  ];

  ngOnInit() {
    this.sharedService.bs.subscribe(item => {
      if (window.location.pathname == '/dashboard/applications') {
        this.applicationCharts();
      }
    });
  }

  changeResource(resource) {
    this.selectedResource = resource.viewValue;
    this.selectedResourceValue = resource.value;
  }

  selectedBar(eve) {

  }

  openModal(templateId, expandedChart) {
    this.expandedChart = expandedChart;
    this.modalService.open(templateId, { size: 'lg', windowClass: 'modal-xxl', backdropClass: 'light-blue-backdrop' });
  }

  applicationCharts() {
    const self = this;
    this.applicationService.memory().then(data => {
      this.memoryData = {
        chartType: 'ColumnChart',
        dataTable: data,
        options: applicationChartOptions.memory
      };
      this.expandedMemoryData = {
        chartType: 'ColumnChart',
        dataTable: data,
        options: applicationChartOptions.memoryExpanded
      };
    })

    this.applicationService.cpuUsageInfo().then(data => {
      this.cpuUsageData = {
        chartType: 'ColumnChart',
        dataTable: data,
        options: applicationChartOptions.cpuUsage
      };
      this.expandedCpuUsageData = {
        chartType: 'ColumnChart',
        dataTable: data,
        options: applicationChartOptions.cpuUsageExpanded
      };
    });

    this.applicationService.containerInfo().then(data => {
      this.containerInfoData = {
        chartType: 'PieChart',
        dataTable: data,
        options: applicationChartOptions.containerInfo,
      };
    });


    this.applicationService.containerCount().then(data => {
      this.containerCountData = {
        chartType: 'ColumnChart',
        dataTable: data,
        options: applicationChartOptions.containerCount
      };
      this.expandedContainerCountData = {
        chartType: 'ColumnChart',
        dataTable: data,
        options: applicationChartOptions. expandedContainerCount
      };
    })
  }

}
