
import { Component, OnInit } from '@angular/core';
import { ChartsService } from '../../charts/charts.service';
import { chartOptions } from '../../charts/chart-options';

import { AuditService } from '../../services/audit.service'
import { auditChartOptions } from '../../options/audit-chart-options'
import { SharedService } from '../../services/shared.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-audit',
  templateUrl: './audit.component.html',
  styleUrls: ['./audit.component.scss']
})
export class AuditComponent implements OnInit {
  expandedChart: string = '';
  fileActions = {};
  topCreated = {};
  topDeleted = {};
  topUpdated = {};
  expandedFileActions={};
  chartList = { fileActions: "File Actions", topCreated: "Top Created Files", topDeleted: "Top Deleted Files", topUpdated: "Top Updated Files" }
  constructor(private sharedService: SharedService, private chartsService: ChartsService, private auditService: AuditService, private modalService: NgbModal) { }

  ngOnInit() {

    this.sharedService.bs.subscribe(item => {
      if (window.location.pathname == '/dashboard/audit') {
        this.auditCharts();
      }
    });

  }

  auditCharts(){
    this.auditService.fileActions().then(data => {
      this.fileActions = {
        chartType: 'ColumnChart',
        dataTable: data,
        options: auditChartOptions.fileActions,
      };
      this.expandedFileActions = {
        chartType: 'ColumnChart',
        dataTable: data,
        options: auditChartOptions.Actions,
      };
    });

    this.auditService.topCreated().then(data => {
      this.topCreated = {
        chartType: 'PieChart',
        dataTable: data,
        options: auditChartOptions.topCreated,
      };
    });

    this.auditService.topDeleted().then(data => {
      this.topDeleted = {
        chartType: 'PieChart',
        dataTable: data,
        options: auditChartOptions.topDeleted,
      };
    });

    this.auditService.topUpdated().then(data => {
      this.topUpdated = {
        chartType: 'PieChart',
        dataTable: data,
        options: auditChartOptions.topUpdated,
      };
    });
  }
  selectedBar($event) {

  }

  openModal(templateId, expandedChart) {
    this.expandedChart = expandedChart;
    this.modalService.open(templateId, { size: 'lg', windowClass: 'modal-xxl', backdropClass: 'light-blue-backdrop' });
  }
}
