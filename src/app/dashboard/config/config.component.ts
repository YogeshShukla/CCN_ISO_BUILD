import { Component, OnInit } from '@angular/core';
import { ChartsService } from '../../charts/charts.service';
import { chartOptions } from '../../charts/chart-options';

import { AwsconfigService } from '../../services/awsconfig.service'
import { awsConfigChartOptions } from '../../options/awsconfig-chart-options'
import { SharedService } from '../../services/shared.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.scss']
})
export class ConfigComponent implements OnInit {
  storageChartData = {};
  knowledgeBaseData = {};

  configImageId = {};
  deletedOnTermination = {};
  resourceType = {};
  resourceCreation = {};
  expandedResourceCreation={};

  selectedResource: string = 'Resource';
  selectedResourceValue: string = '';
  expandedChart: string = '';
  //chartList = { configImageId: "Configuration Image Id", deletedOnTermination: "Volumes Deleted On Instance Termination", resourceType: "Resource Type", resourceCreation: "Resource Creation" }
  chartList = { configImageId: "Configuration Image Id Sizes", deletedOnTermination: "Volumes Configuration On Instance Termination", resourceType: "Utilized AWS Resource", resourceCreation: "AWS Resources Creation" }
  constructor(private sharedService: SharedService, private chartsService: ChartsService, private awsService: AwsconfigService, private modalService: NgbModal) { }

  resources = [
    { value: 'resource1', viewValue: 'Resource 1' },
    { value: 'resource2', viewValue: 'Resource 2' },
    { value: 'resource3', viewValue: 'Resource 3' },
  ];

  ngOnInit() {
    this.sharedService.bs.subscribe(item => {
      if (window.location.pathname == '/dashboard/config') {
        this.awsConfigCharts();
      }
    });
  }

  selectedBar($event) {

  }

  changeResource(resource) {
    this.selectedResource = resource.viewValue;
    this.selectedResourceValue = resource.value;
  }

  openModal(templateId, expandedChart) {
    this.expandedChart = expandedChart;
    this.modalService.open(templateId, { size: 'lg', windowClass: 'modal-xxl', backdropClass: 'light-blue-backdrop' });
  }

  awsConfigCharts() {
    const self = this;

    this.awsService.configImageId().then(data => {
      this.configImageId = {
        chartType: 'PieChart',
        dataTable: data,
        options: awsConfigChartOptions.configImageId,
      };
    });

    this.awsService.deletedOnTermination().then(data => {
      this.deletedOnTermination = {
        chartType: 'PieChart',
        dataTable: data,
        options: awsConfigChartOptions.deletedOnTermination,
      };
    });

    this.awsService.resourceType().then(data => {
      this.resourceType = {
        chartType: 'PieChart',
        dataTable: data,
        options: awsConfigChartOptions.resourceType,
      };
    });

    this.awsService.resourceCreation().then(data => {
      this.resourceCreation = {
        chartType: 'ColumnChart',
        dataTable: data,
        options: awsConfigChartOptions.resourceCreation,
      };
      this.expandedResourceCreation = {
        chartType: 'ColumnChart',
        dataTable: data,
        options: awsConfigChartOptions.expandedCreation,
      };
    });
  }

}
