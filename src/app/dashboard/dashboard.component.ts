import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartsService } from '../charts/charts.service';
import { chartOptions } from '../charts/chart-options';
import { PatternDetectionDetailsComponent } from '../charts/pattern-detection-details/pattern-detection-details.component';
import { Route, Router } from '@angular/router';

import { SharedService } from '../services/shared.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  host: {
    '(document:click)': 'docClick($event)',
  }
})
export class DashboardComponent implements OnInit {
  @ViewChild(PatternDetectionDetailsComponent)
  private patternDetectionDetailsComponent: PatternDetectionDetailsComponent;
  constructor(private sharedService: SharedService, private chartsService: ChartsService, private router: Router) {
    this.router.navigate(['/dashboard/infrastructure']);
  }
  knowledgeBaseData = {};
  pieChartLoaded: boolean = false;
  memoryChartData = {};
  cpuChartData = {};
  cpuChartError: Boolean = false;
  storageChartData = {};
  pdrDate: string = '';
  guageChartData: any = {};
  guageChartData2: any = {};
  networkData: any = {};
  columnChartData: any = {};
  patternDetectionData = {};
  selectedAccount: string = 'Account';
  selectedAccountValue;

  knowledgeBaseLoaded = false;
  isDateDisabled=true;
  disableDate(bool=false){
    this.isDateDisabled=bool

  }

  accounts = [
    { value: 'azure', viewValue: 'Azure' },
    { value: 'aws', viewValue: 'AWS' }
  ]
  public selectedMoments = [new Date(2018, 0, 1, 0, 0), new Date()];
  ngOnInit() {
    
  }

  selectedBar(value) {
    this.chartsService.pdrData().then(data => {
      this.pdrDate = data['xAxis'][value]['date'];
    }).catch((err) => {
      this.pdrDate = null;
    })
    this.patternDetectionDetailsComponent.chartBarSelected(value);
  }

  changeAccount(account){
    if(account != ''){
      this.selectedAccount = account.viewValue;
      this.selectedAccountValue = account.value;
    } else {
      this.selectedAccount = 'Account';
      this.selectedAccountValue = '';
    } 
  }

  docClick(eve){
      if(eve.srcElement.className == 'owl-dt-control-button-content' && eve.srcElement.innerText=='Set'){
          if(this.selectedMoments[0] !=null && this.selectedMoments[1] !=null){
            this.sharedService.newDateRangeSelected({startDate:this.selectedMoments[0].getTime(),endDate:this.selectedMoments[1].getTime()})
          }
      }
    }
  }
