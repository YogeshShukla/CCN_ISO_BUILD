
import { Component, OnInit ,ViewChild } from '@angular/core';
//import { ChartsService } from '../../charts/charts.service';
import { chartOptions } from '../../charts/chart-options';

import { FinancialsService } from '../../services/financials.service';
import { financialsChartOptions } from '../../options/financials-chart-options';
import { SharedService } from '../../services/shared.service';

import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-financials',
  templateUrl: './financials.component.html',
  styleUrls: ['./financials.component.scss']
})
export class FinancialsComponent implements OnInit {
  @ViewChild('fchart') fchart;
  selectedResource: string = 'Resource';
  selectedResourceValue: string = '';

  myChart = {};

  expandedChart: string = '';
  ec2InstanceCost = {};
  awsAcctotalCost = {};
  s3reqCost = {};
  separateServiceCost = {};
  showLoader = true;
  isError = false;
  chartList = { ec2InstanceCost: "EC2 Instances Cost", s3reqCost: "Amazon S3 Request Cost", separateServiceCost: "Separate Service Cost", awsAccTotalCost: "AWS Account Total Cost" }
  //AWS Price Statistics
  constructor(private sharedService: SharedService, private financialServices: FinancialsService, private modalService: NgbModal) { }

  resources = [
    { value: 'resource1', viewValue: 'Resource 1' },
    { value: 'resource2', viewValue: 'Resource 2' },
    { value: 'resource3', viewValue: 'Resource 3' },
  ];

  ngOnInit() {
    /*
    let tempData = [
      ['Location', 'Parent', 'Size'],
      ['AWS TotalCost', null, 10],
      ['S3 Requests', 'AWS TotalCost', 20],
      ['EC2 Instances', 'AWS TotalCost', 20],
      ['AWS Supprt Bussiness', 'AWS TotalCost', 10],
      ['Amazon VPC', 'AWS TotalCost', 10],
      ['AWS Cloud Trail', 'AWS TotalCost', 10],
      ['AWS COnfig', 'AWS TotalCost', 10],
      ['Amazon Cloud Watch', 'AWS TotalCost', 10],

      ['Spot EC2 Instances', 'EC2 Instances', 50],
      ['OnDemand EC2 Instances', 'EC2 Instances', 50],

      ['API Request', 'S3 Requests', 33],
      ['Data Transfer', 'S3 Requests', 33],
      ['Storage', 'S3 Requests', 33],

    ];
    */

    
    this.myChart = {};

    //this.financialCharts();
    this.sharedService.bs.subscribe(item => {
      if (window.location.pathname == '/dashboard/financials') {
        this.financialCharts();
      }
    });
  }

  changeResource(resource) {
    this.selectedResource = resource.viewValue;
    this.selectedResourceValue = resource.value;
  }

  openModal(templateId, expandedChart) {
    this.expandedChart = expandedChart;
    this.modalService.open(templateId, { size: 'lg', windowClass: 'modal-xxl', backdropClass: 'light-blue-backdrop' });
  }

  financialCharts() {
    // this.financialServices.ec2InstancesCost().then(data => {
    //   this.ec2InstanceCost = {
    //     chartType: 'PieChart',
    //     dataTable: data,
    //     options: financialsChartOptions.ec2InstancesCost,
    //     formatters: [financialsChartOptions.formatters]
    //   };
    // });

    // this.financialServices.awsAccTotalCost().then(data => {
    //   this.awsAcctotalCost = {
    //     chartType: 'PieChart',
    //     dataTable: data,
    //     options: financialsChartOptions.awsAccTotalCost,
    //     formatters: [financialsChartOptions.formatters]
    //   };
    // });

    // this.financialServices.s3reqCost().then(data => {
    //   this.s3reqCost = {
    //     chartType: 'ColumnChart',
    //     dataTable: data,
    //     options: financialsChartOptions.s3reqCost,
    //     formatters: [financialsChartOptions.formatters]
    //   };
    // });

    // this.financialServices.separateServiceCost().then(data => {
    //   this.separateServiceCost = {
    //     chartType: 'ColumnChart',
    //     dataTable: data,
    //     options: financialsChartOptions.separateServiceCost,
    //     formatters: [financialsChartOptions.formatters]
    //   };
    // });

    this.financialServices.financials(item=>{
        if(item.length>1){
          this.myChart = {
            chartType: 'TreeMap',
            displayed: true,
            dataTable: item,
            options: {
              backgroundColor: {
                fill: 'transparent'
              },
              minColor: '#E0301E',
              midColor: '#FFB600',
              maxColor: '#A32020',
              noColor: '#FF0000',
              headerColor: '#EB8C00',
              fontColor: 'white',
              width: '100%',
              height: '500',
              showScale: false,
              showTooltips: true,
              forceIFrame:true
            }
          }
          this.isError = false;
        }
        else{
          this.isError = true;
        }
     
    });
  }

  goUpAndDraw(){
  console.log("My check");
  let googleChartWrapper = this.fchart.wrapper;
  console.log(this.fchart)
  this.fchart.redraw(); 

  
  }

  ready(eve){
    this.showLoader = false;
  }

  error(eve){
    this.showLoader =false; 
    this.isError = true;
  }
}
