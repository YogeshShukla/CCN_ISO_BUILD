import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ChartsService } from '../../charts/charts.service';
import { InfrastructureService } from '../../services/infrastructure.service';
import { chartOptions } from '../../charts/chart-options';
import { SharedService } from '../../services/shared.service';
import { infraChartOptions } from '../../options/infra-chart-options';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-infrastructure',
  templateUrl: './infrastructure.component.html',
  styleUrls: ['./infrastructure.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class InfrastructureComponent implements OnInit {
  cpuChartData = {}

  storageChartData = {};
  networkChartData = {};
  memoryChartData = {};
  expandNetworkChartData={};
  memoryExpandedChartData={};
  storageExpandedChartData={};
  knowledgeBaseData = {};
  knowledgeBaseLoaded = false;
  selectedResource: string = 'Resource';
  selectedResourceValue: string = '';
  expandedChart: string = '';
  chartList = { cpu: "CPU Usage", network: "Network Traffic", memory: "Memory Consumption", storage: "Storage Retention" };

  constructor(private sharedService: SharedService, private chartsService: ChartsService, private infra: InfrastructureService, private modalService: NgbModal) { }

  resources = [
    { value: 'resource1', viewValue: 'Resource 1' },
    { value: 'resource2', viewValue: 'Resource 2' },
    { value: 'resource3', viewValue: 'Resource 3' },
  ];

  ngOnInit() {
    this.sharedService.bs.subscribe(item => {
      if(window.location.pathname=='/dashboard/infrastructure'){
        this.infrastructureCharts();
      }
      
    });
  }

  changeResource(resource) {
    this.selectedResource = resource.viewValue;
    this.selectedResourceValue = resource.value;
  }

  openModal(templateId, expandedChart) {
    this.expandedChart = expandedChart;
    this.modalService.open(templateId, { size: 'lg', windowClass: 'modal-xxl', backdropClass: 'light-blue-backdrop' });
  }

  infrastructureCharts() {
    const self = this;
    this.infra.storage().then(data => {

      self.storageChartData = {
        chartType: 'ColumnChart',
        dataTable: data,
        options: infraChartOptions.storage
      };
      self.storageExpandedChartData = {
        chartType: 'ColumnChart',
        dataTable: data,
        options: infraChartOptions.storageExpanded
      };
    })

    this.infra.memory().then(data => {
      self.memoryChartData = {
        chartType: 'ColumnChart',
        dataTable: data,
        options: infraChartOptions.memory
      };
      self.memoryExpandedChartData = {
        chartType: 'ColumnChart',
        dataTable: data,
        options: infraChartOptions.memoryExpanded
      };
    })



    this.infra.cpu().then(data => {
      self.cpuChartData = {
        chartType: 'PieChart',
        dataTable: data,
        options: infraChartOptions.cpu,
      };
    });

    this.infra.network().then(data => {
      self.networkChartData = {
        chartType: 'ColumnChart',
        dataTable: data,
        options: infraChartOptions.network
      };
      self.expandNetworkChartData = {
        chartType: 'ColumnChart',
        dataTable: data,
        options: infraChartOptions.networkExpanded
      };
    })
  }

}
