import { osPatchesOption } from '../../options/osPatches-chart-option';
import { Component } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { osPatchesService } from '../../services/osPatches.service';
import { SharedService } from '../../services/shared.service';



@Component({
  selector: 'app-osPatches',
  templateUrl: './osPatches.component.html',
  styleUrls: ['./osPatches.component.scss']
})
export class osPatchesComponent {
count = 0
constructor(private osPatchesService: osPatchesService) {}
criticalPatchesData = []
importantPatchesData = []
moderatePatchesData = []
unspecifiedPatchesData =[]
showLoader=true;


ngOnInit() {
  this.osPatchesService.patchesApi().then((res) =>{
  
    this.showLoader=false
    
    let response = JSON.parse(res.body)
    let allPatchesData = response.hits.hits[0]._source.Patches

    this.importantPatchesData = allPatchesData.filter((item)=>{
         return item.MsrcSeverity == 'Important'
         
    });   
this.criticalPatchesData = allPatchesData.filter((item)=>{
  return item.MsrcSeverity == 'Critical'         
         
});
this.moderatePatchesData = allPatchesData.filter((item)=>{
  return item.MsrcSeverity == 'Moderate'         
         
});
this.unspecifiedPatchesData = allPatchesData.filter((item)=>{
  return item.MsrcSeverity == 'Unspecified'
           
});
});
    }
setActive(count) {
    this.count = count
  }
 checkActive(count) {
    return this.count == count ? "patches-active" : ""
  }
}
 









