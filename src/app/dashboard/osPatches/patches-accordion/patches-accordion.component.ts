import { Component, Input } from '@angular/core';

@Component({
    selector: 'patches-accordion',
    templateUrl: './patches-accordion.component.html',
    styleUrls: ['./patches-accordion.scss']
})
export class osPatchesAccordionComponent {
    @Input() PatchesData = []
    

    accordionText(i) {
        return "accordion" + i
    }
    accordionValue(i) {
        return this["accordion" + i]
    }
    toggleAccordion(value) {
        if (this[value]) this[value] = false
        else this[value] = true
    }
}