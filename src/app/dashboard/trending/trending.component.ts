import { Component, OnInit } from '@angular/core';
import { ChartsService } from '../../charts/charts.service';
import { chartOptions } from '../../charts/chart-options';
import { TrendingService } from '../../services/trending.service';
import { trendingChartOptions } from '../../options/trending-chart-options';
import { SharedService } from '../../services/shared.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-trending',
  templateUrl: './trending.component.html',
  styleUrls: ['./trending.component.scss']
})
export class TrendingComponent implements OnInit {

  topHostsCpu = {};
  topHostsMemory = {};
  topProcessCpu = {};
  topProcessMemory = {};
  expandedTopHostsCpu={};
  expandedTopHostsMemory={};
  expandedTopProcessCpu={};
  expandedTopProcessMemory={};

  selectedResource:string = 'Resource';
  selectedResourceValue:string = '';
  expandedChart: string = '';
  //chartList = { topHostsCpu: "Top hosts by CPU for system", topHostsMemory: "Top hosts by Memory for system", topProcessCpu: "Top Process by CPU", topProcessMemory: "Top Process by Memory" }
  chartList = { topHostsCpu: "Hosts CPU Usage", topHostsMemory: "Hosts Memory Consumption", topProcessCpu: "Processes CPU Time Usage", topProcessMemory: "Processes Memory Consumption" }
  constructor(private chartsService: ChartsService, private trendingService:TrendingService,private sharedService:SharedService, private modalService: NgbModal) { }

  resources = [
    { value: 'resource1', viewValue: 'Resource 1' },
    { value: 'resource2', viewValue: 'Resource 2' },
    { value: 'resource3', viewValue: 'Resource 3' },
  ];

  ngOnInit() {
    // this.sharedService.bs.subscribe(item => {
    //   if (window.location.pathname == '/dashboard/trending') {
    //     this.trendingCharts();
    //   }
    // });

    this.trendingCharts();
  }

  selectedBar($event){

  }

  trendingCharts(){
      this.trendingService.topHostsCpu().then(data=>{
        this.topHostsCpu = {
          chartType: 'LineChart',
          dataTable: data,
          options: trendingChartOptions.topHostsCpu
        };
        this.expandedTopHostsCpu = {
          chartType: 'LineChart',
          dataTable: data,
          options: trendingChartOptions.expandedTopHostsCpu
        };
      })

      this.trendingService.topHostsMemory().then(data=>{
        this.topHostsMemory = {
          chartType: 'LineChart',
          dataTable: data,
          options: trendingChartOptions.topHostsMemory
        };
        this.expandedTopHostsMemory = {
          chartType: 'LineChart',
          dataTable: data,
          options: trendingChartOptions.expandedTopHost
        };
      })

      this.trendingService.topProcessCpu().then(data=>{
        this.topProcessCpu = {
          chartType: 'LineChart',
          dataTable: data,
          options: trendingChartOptions.topProcessCpu
        };
        this.expandedTopProcessCpu = {
          chartType: 'LineChart',
          dataTable: data,
          options: trendingChartOptions.expandedProcessCpu
        };
      })

      this.trendingService.topProcessMemory().then(data=>{
        this.topProcessMemory = {
          chartType: 'LineChart',
          dataTable: data,
          options: trendingChartOptions.topProcessMemory
        };
        this.expandedTopProcessMemory = {
          chartType: 'LineChart',
          dataTable: data,
          options: trendingChartOptions.expandedProcessMemory
        };
      })
  }

  changeResource(resource){
    this.selectedResource = resource.viewValue;
    this.selectedResourceValue = resource.value;
  }

  openModal(templateId, expandedChart) {
    this.expandedChart = expandedChart;
    this.modalService.open(templateId, { size: 'lg', windowClass: 'modal-xxl', backdropClass: 'light-blue-backdrop' });
  }

}
