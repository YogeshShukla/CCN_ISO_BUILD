import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header-nav',
  templateUrl: './header-nav.component.html',
  styleUrls: ['./header-nav.component.scss']
})
export class HeaderNavComponent implements OnInit {
  username=localStorage.getItem('username')

 
  constructor(private router: Router) { }
  ngOnInit() {
  }
  signOut(){
     this.router.navigateByUrl('/login');

  }

}
