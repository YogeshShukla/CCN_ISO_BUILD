import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router';
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent {
    title = 'app';
    loginForm: FormGroup
   
    testUsers = [
        {
            email: "dhivya@pwc.com",
            password: "Welcome@123",
            name: "Dhivya",

        },
        {
            email: "arun@pwc.com",
            password: "Welcome@123",
            name: "Arun",
        },
        {
            email: "mansoor@pwc.com",
            password: "Welcome@123",
            name: "Mansoor",
        }
    ]
    constructor(private router: Router, private fb: FormBuilder) {
        this.loginForm = this.fb.group({
            email: ["arun@pwc.com", Validators.required],
            password: ["Welcome@123", Validators.required],

        })
    }
    navigate() {
        this.testUsers.forEach((item)=>{
           
            if(this.loginForm.value.email == item.email && this.loginForm.value.password == item.password){
                this.router.navigateByUrl('/dashboard');
                localStorage.setItem("username", item.name);
            }
        })
    this.loginForm.reset()

    }
}
