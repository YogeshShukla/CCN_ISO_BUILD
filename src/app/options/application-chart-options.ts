export const applicationChartOptions = {
    containerCount: {
        chartArea: { width: '85%', height: '70%' },
        width:550,
        legend: {
            textStyle: { color: 'black' },
            position: 'bottom',
            alignment: 'center'
        },
        bar: {
            groupWidth: 30
        },
        hAxis: {
            textStyle: { color: 'black' }
        },
        vAxis: {
            textStyle: { color: 'black' }
        },
        backgroundColor: '#FFFFFF',
        colors: ['#a32020']
    },
    expandedContainerCount: {
        chartArea: { width: '85%', height: '70%' },
        width:950,
        legend: {
            textStyle: { color: 'black' },
            position: 'bottom',
            alignment: 'center'
        },
        bar: {
            groupWidth: 30
        },
        hAxis: {
            textStyle: { color: 'black' }
        },
        vAxis: {
            textStyle: { color: 'black' }
        },
        backgroundColor: '#FFFFFF',
        colors: ['#a32020']
    },
    containerInfo: {
        pieHole: 0.45,
        chartArea: { width: '85%', height: '70%' },
        backgroundColor: '#FFFFFF',
        colors: ['#ffb600', '#eb8c00', '#dc6900', '#a32020', '#602320'],
        legend: {
            alignment: 'center',
            textStyle: { color: 'black', fontSize: 14 }
        },
        pieSliceText: 'none',
        tooltip: { ignoreBounds: false },
        pieSliceBorderColor: 'none'
    },
    memory: {
        chartArea: { width: '85%', height: '70%' },
        width:550,
        legend: {
            textStyle: { color: 'black' },
            position: 'bottom',
            alignment: 'center'
        },
        bar: {
            groupWidth: 30
        },
        hAxis: {
            textStyle: { color: 'black' }
        },
        vAxis: {
            format: 'short',
            textStyle: { color: 'black' }
        },
        backgroundColor: '#FFFFFF',
        colors: ['#a32020', '#eb8c00', '#dc6900', '#602320', '#ffb600']
    },
    memoryExpanded: {
        chartArea: { width: '85%', height: '70%' },
        width:950,
        legend: {
            textStyle: { color: 'black' },
            position: 'bottom',
            alignment: 'center'
        },
        bar: {
            groupWidth: 30
        },
        hAxis: {
            textStyle: { color: 'black' }
        },
        vAxis: {
            format: 'short',
            textStyle: { color: 'black' }
        },
        backgroundColor: '#FFFFFF',
        colors: ['#a32020', '#eb8c00', '#dc6900', '#602320', '#ffb600']
    },
    cpuUsage: {
        chartArea: { width: '85%', height: '70%' },
        width:550,
        legend: {
            textStyle: { color: 'black' },
            position: 'bottom',
            alignment: 'center'
        },
        bar: {
            groupWidth: 30
        },
        hAxis: {
            textStyle: { color: 'black' }
        },
        vAxis: {
            format: 'short',
            textStyle: { color: 'black' }
        },
        backgroundColor: '#FFFFFF',
        colors: ['#ffb600', '#a32020', '#eb8c00', '#dc6900', '#602320']
    },
    cpuUsageExpanded: {
        chartArea: { width: '85%', height: '70%' },
        width:950,
        legend: {
            textStyle: { color: 'black' },
            position: 'bottom',
            alignment: 'center'
        },
        bar: {
            groupWidth: 30
        },
        hAxis: {
            textStyle: { color: 'black' }
        },
        vAxis: {
            format: 'short',
            textStyle: { color: 'black' }
        },
        backgroundColor: '#FFFFFF',
        colors: ['#ffb600', '#a32020', '#eb8c00', '#dc6900', '#602320']
    }
}
