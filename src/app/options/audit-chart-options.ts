export const auditChartOptions = {
    fileActions: {
        chartArea: { width: '85%', height: '70%' },
        width:550,
        legend: {
            textStyle: { color: 'black' },
            position: 'bottom',
            alignment: 'center'
        },
        bar: {
            groupWidth: 30
        },
        hAxis: {
            textStyle: { color: 'black' }
        },
        vAxis: {
            textStyle: { color: 'black' }
        },
        backgroundColor: '#FFFFFF',
        colors: ['#ffb900']
    },
    Actions: {
        chartArea: { width: '85%', height: '70%' },
        width:950,
        legend: {
            textStyle: { color: 'black' },
            position: 'bottom',
            alignment: 'center'
        },
        bar: {
            groupWidth: 30
        },
        hAxis: {
            textStyle: { color: 'black' }
        },
        vAxis: {
            textStyle: { color: 'black' }
        },
        backgroundColor: '#FFFFFF',
        colors: ['#ffb900']
    },
    topCreated: {
        pieHole: 0.45,
        chartArea: { width: '85%', height: '70%' },
        backgroundColor: '#FFFFFF',
        colors: ['#ffb600', '#eb8c00', '#dc6900', '#a32020', '#602320'],
        legend: {
            alignment: 'center',
            textStyle: { color: 'black', fontSize: 14 }
        },
        pieSliceText: 'none',
        tooltip: { ignoreBounds: false },
        pieSliceBorderColor: 'none'
    },
    topDeleted : {
        pieHole: 0.45,
        chartArea: { width: '85%', height: '70%' },
        backgroundColor: '#FFFFFF',
        colors: ['#ffb600', '#eb8c00', '#dc6900', '#a32020', '#602320'],
        legend: {
            alignment: 'center',
            textStyle: { color: 'black', fontSize: 14 }
        },
        pieSliceText: 'none',
        tooltip: { ignoreBounds: false },
        pieSliceBorderColor: 'none'
    },
    topUpdated: {
        pieHole: 0.45,
        chartArea: { width: '85%', height: '70%' },
        backgroundColor: '#FFFFFF',
        colors: ['#dc6900','#ffb600', '#eb8c00', '#a32020', '#602320'],
        legend: {
            alignment: 'center',
            textStyle: { color: 'black', fontSize: 14 }
        },
        pieSliceText: 'none',
        tooltip: { ignoreBounds: false },
        pieSliceBorderColor: 'none'
    },
}
