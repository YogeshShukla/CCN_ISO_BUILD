export const financialsChartOptions = {
  ec2InstancesCost: {
    pieHole: 0.45,
    chartArea: { width: '85%', height: '70%' },
    backgroundColor: '#FFFFFF',
    colors: ['#ffb600', '#eb8c00', '#dc6900', '#a32020', '#602320'],
    legend: {
      alignment: 'center',
      textStyle: { color: 'black', fontSize: 14 }
    },
    pieSliceText: 'none',
    tooltip: { ignoreBounds: false },
    pieSliceBorderColor: 'none',
    prefix: '$'
  },
  awsAccTotalCost: {
    pieHole: 0.45,
    chartArea: { width: '85%', height: '70%' },
    backgroundColor: '#FFFFFF',
    colors: ['#a32020', '#ffb600', '#eb8c00', '#dc6900', '#602320'],
    legend: {
      alignment: 'center',
      textStyle: { color: 'black', fontSize: 14 }
    },
    pieSliceText: 'none',
    tooltip: { ignoreBounds: false },
    pieSliceBorderColor: 'none'
  },
  s3reqCost: {
    chartArea: { width: '85%', height: '70%' },
    legend: {
      textStyle: { color: 'black' },
      position: 'bottom',
      alignment: 'center'
    },
    bar: {
      groupWidth: 30
    },
    hAxis: {
      textStyle: { color: 'black' }
    },
    vAxis: {
      textStyle: { color: 'black' }
    },
    backgroundColor: '#FFFFFF',
    colors: ['#ffb900']
  },
  separateServiceCost:{
    chartArea: { width: '85%', height: '70%' },
    legend: {
      textStyle: { color: 'black' },
      position: 'bottom',
      alignment: 'center'
    },
    bar: {
      groupWidth: 30
    },
    hAxis: {
      textStyle: { color: 'black' }
    },
    vAxis: {
      format: 'short',
      textStyle: { color: 'black' }
    },
    backgroundColor: '#FFFFFF',
    colors: ['#ffb900']
  },
  formatters: {
    columns: [1],
    type: 'NumberFormat',
    options: {
      prefix: '$'
    }
  }

}