export const osPatchesOption = {
    available: {
     pieHole: 0.45,
    chartArea: { width: '85%', height: '70%' },
    backgroundColor: '#FFFFFF',
    colors: ['#ffb600', '#eb8c00', '#dc6900', '#a32020', '#602320'],
    legend: {
      alignment: 'center',
      textStyle: { color: 'black', fontSize: 14 }
    },
    pieSliceText: 'none',
    tooltip: { ignoreBounds : false },
    pieSliceBorderColor: 'none'
  },
  // critical: {
  //   chartArea: { width: '85%', height: '70%' },
  //   legend: {
  //     textStyle: { color: 'white' },
  //     position: 'bottom',
  //     alignment: 'center'
  //   },
  //   bar: {
  //     groupWidth: 30
  //   },
  //   hAxis: {
  //     textStyle: { color: 'white' }
  //   },
  //   vAxis: {
  //     format: 'short',
  //     textStyle: { color: 'white' }
  //   },
  //   backgroundColor: '#2d3035',
  //   colors: ['#ffc82d', '#a32020', '#dc6900', '#602320', '#ffb600']
  // },

}


