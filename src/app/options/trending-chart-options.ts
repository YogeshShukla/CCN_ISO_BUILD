export const trendingChartOptions = {
    topHostsCpu: {
        series: {
            pointsVisible: true
        },
        legend: {
            position: 'bottom',
            alignment: 'center',
            textStyle: { color: 'black' }
        },
        hAxis: {
            textStyle: { color: 'black' }
        },
        vAxis: {
            textStyle: { color: 'black' }
        },
        chartArea: { width: '80%', height: '70%' },
        width:550,
        backgroundColor: '#FFFFFF',
        colors: ['#ffc82d', '#a32020', '#dc6900', '#602320', '#ffb600']
    },
    expandedTopHostsCpu: {
        series: {
            pointsVisible: true
        },
        legend: {
            position: 'bottom',
            alignment: 'center',
            textStyle: { color: 'black' }
        },
        hAxis: {
            textStyle: { color: 'black' }
        },
        vAxis: {
            textStyle: { color: 'black' }
        },
        chartArea: { width: '80%', height: '70%' },
        width:950,
        backgroundColor: '#FFFFFF',
        colors: ['#ffc82d', '#a32020', '#dc6900', '#602320', '#ffb600']
    },
    topHostsMemory: {
        series: {
            pointsVisible: true
        },
        legend: {
            position: 'bottom',
            alignment: 'center',
            textStyle: { color: 'black' }
        },
        hAxis: {
            textStyle: { color: 'black' }
        },
        vAxis: {
            textStyle: { color: 'black' }
        },
        chartArea: { width: '80%', height: '70%' },
        width:550,
        backgroundColor: '#FFFFFF',
        colors: ['#ffc82d', '#a32020', '#dc6900', '#602320', '#ffb600']
    },
    expandedTopHost: {
        series: {
            pointsVisible: true
        },
        legend: {
            position: 'bottom',
            alignment: 'center',
            textStyle: { color: 'black' }
        },
        hAxis: {
            textStyle: { color: 'black' }
        },
        vAxis: {
            textStyle: { color: 'black' }
        },
        chartArea: { width: '80%', height: '70%' },
        width:950,
        backgroundColor: '#FFFFFF',
        colors: ['#ffc82d', '#a32020', '#dc6900', '#602320', '#ffb600']
    },
    
    
    topProcessCpu: {
        series: {
            pointsVisible: true
        },
        legend: {
            position: 'bottom',
            alignment: 'center',
            textStyle: { color: 'black' }
        },
        hAxis: {
            textStyle: { color: 'black' }
        },
        vAxis: {
            textStyle: { color: 'black' }
        },
        chartArea: { width: '80%', height: '70%' },
        width:550,
        backgroundColor: '#FFFFFF',
        colors: ['#ffc82d', '#a32020', '#dc6900', '#602320', '#ffb600']
    },
    expandedProcessCpu: {
        series: {
            pointsVisible: true
        },
        legend: {
            position: 'bottom',
            alignment: 'center',
            textStyle: { color: 'black' }
        },
        hAxis: {
            textStyle: { color: 'black' }
        },
        vAxis: {
            textStyle: { color: 'black' }
        },
        chartArea: { width: '80%', height: '70%' },
        width:950,
        backgroundColor: '#FFFFFF',
        colors: ['#ffc82d', '#a32020', '#dc6900', '#602320', '#ffb600']
    },
    topProcessMemory: {
        series: {
            pointsVisible: true
        },
        legend: {
            position: 'bottom',
            alignment: 'center',
            textStyle: { color: 'black' }
        },
        hAxis: {
            textStyle: { color: 'black' }
        },
        vAxis: {
            textStyle: { color: 'black' }
        },
        chartArea: { width: '80%', height: '70%' },
        width:550,
        backgroundColor: '#FFFFFF',
        colors: ['#ffc82d', '#a32020', '#dc6900', '#602320', '#ffb600']
    },
    expandedProcessMemory: {
        series: {
            pointsVisible: true
        },
        legend: {
            position: 'bottom',
            alignment: 'center',
            textStyle: { color: 'black' }
        },
        hAxis: {
            textStyle: { color: 'black' }
        },
        vAxis: {
            textStyle: { color: 'black' }
        },
        chartArea: { width: '80%', height: '70%' },
        width:950,
        backgroundColor: '#FFFFFF',
        colors: ['#ffc82d', '#a32020', '#dc6900', '#602320', '#ffb600']
    }
}
