import { Component, Input, Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { config } from './config';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { SharedService } from './shared.service'

@Injectable()
export class ApplicationService {

  constructor(private http: Http, private sharedService: SharedService) { }

  public containerCount() {
    return this.http.post(`${config.nodeUri}/applications`, { "target": "containerCount", "startDate": this.sharedService.dateObj['startDate'], "endDate": this.sharedService.dateObj['endDate'] })
      .map(res => this.convertContainerCountData(res.json())).toPromise();

  }

  convertContainerCountData(resp) {
    let containerData = [];
    let titles = ['Title'];
    titles = titles.concat('No of Containers');
    if (resp['aggregations']['2']['value']) {
      containerData.push(titles);
      let running = resp['aggregations']['2']['value'];
      let paused = resp['aggregations']['3']['value'];
      let stopped = resp['aggregations']['4']['value'];
      containerData.push(['Running', running]);
      containerData.push(['Paused', paused]);
      containerData.push(['Stopped', stopped]);
    }
    return containerData;
  }


  public memory() {
    return this.http.post(`${config.nodeUri}/applications`, { "target": "memory", "startDate": this.sharedService.dateObj['startDate'], "endDate": this.sharedService.dateObj['endDate'] })
      .map(res => this.convertMemoryData(res.json())).toPromise();
  }

  convertMemoryData1(resp) {
    let memoryData = [];
    let titles = ['Title'];
    titles = titles.concat('Memory');
    memoryData.push(titles);
    let arr = resp['aggregations']['2']['buckets'][0]['3']['buckets']
    arr.forEach(item => {
      let value = item['1']['value'];
      memoryData.push([item.key, value]);
    })

    return memoryData;
  }

  convertMemoryData(resp) {
    return this.convertCpuUsageData(resp);
  }

  containerInfo() {
    return this.http.post(`${config.nodeUri}/applications`, { "target": "containerInfo", "startDate": this.sharedService.dateObj['startDate'], "endDate": this.sharedService.dateObj['endDate'] })
      .map(res => this.convertContainerInfoData(res.json())).toPromise();
  }

  public convertContainerInfoData(resp) {

    let info = [];
    if (resp['aggregations']['2']['buckets'].length) {
      info.push(['Title', 'Container Info']);

      resp['aggregations']['2']['buckets'].forEach(item => {
        info.push([item.key, item.doc_count])
      })
    }

    return (info)
  }

  cpuUsageInfo() {
    return this.http.post(`${config.nodeUri}/applications`, { "target": "cpuUsage", "startDate": this.sharedService.dateObj['startDate'], "endDate": this.sharedService.dateObj['endDate'] })
      .map(res => this.convertCpuUsageData(res.json())).toPromise();
  }

  convertCpuUsageData1(resp) {
    let info = [];
    let titles = ['Date'];

    resp['aggregations']['2']['buckets'][0]['3']['buckets'].forEach(item => {

      titles = titles.concat(item.key)
    });
    info.push(titles);
    resp['aggregations']['2']['buckets'].forEach(item => {

      if (item['3']['buckets'].length != 0) {

        let dateStr = this.sharedService.dateTimeConverter(item['key_as_string']);

        let fields = [];
        fields = fields.concat(dateStr)

        item['3']['buckets'].forEach(innerItem => {
          fields = fields.concat(innerItem.doc_count);
        })

        info.push(fields);
      }

    })
    return (info)
  }

  convertCpuUsageData(resp) {
    let info = [];
    let titles = [];

    let fields = [];

    let max = 0;

    let k = 0;
    let index = 0;
    resp['aggregations']['2']['buckets'].forEach(item => {
      if (max < item['3']['buckets'].length) {
        max = item['3']['buckets'].length;
        index = k;
      }
      k++;
    });

    resp['aggregations']['2']['buckets'][index]['3']['buckets'].forEach(item => {
      titles = titles.concat(item.key);
    })

    resp['aggregations']['2']['buckets'].forEach(item => {
      if (item['3']['buckets'].length != 0) {
        let innerFields = [];
        innerFields.push(this.sharedService.dateTimeConverter(item.key_as_string)); 
        titles.forEach(kk => {
          let checkPresent = false;
          item['3']['buckets'].forEach(kl => {
            if (kk == kl.key) {
              checkPresent = true;
              innerFields.push(kl.doc_count);
            }
          })

          if (!checkPresent)
            innerFields.push(0);
        })
        fields.push(innerFields)

      }
    })
    titles.unshift('Date');
    info.push(titles);
    fields.forEach(item => {
      info.push(item);
    })
    return (info)
  }

}
