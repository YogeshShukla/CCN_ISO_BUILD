import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { config } from './config';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { SharedService } from '../services/shared.service';

@Injectable()
export class AuditService {

  constructor(private http: Http, private sharedService:SharedService) { }

  public fileActions(){
    return this.http.post(`${config.nodeUri}/audit`, { "target": "fileActions", "startDate": this.sharedService.dateObj['startDate'], "endDate": this.sharedService.dateObj['endDate'] })
    .map(res => this.fileActionsConvert(res.json())).toPromise();
  }

  fileActionsConvert(resp){
    let fileActionsData = [];
    fileActionsData.push(['Title', 'File Actions'])
    resp['aggregations']['2']['buckets'].forEach(item=>{
      fileActionsData.push([item.key, item.doc_count])
    })
    return fileActionsData;
  }

  public topCreated(){
    return this.http.post(`${config.nodeUri}/audit`, { "target": "topCreated", "startDate": this.sharedService.dateObj['startDate'], "endDate": this.sharedService.dateObj['endDate'] })
    .map(res => this.topCreatedConvert(res.json())).toPromise();
  }

  topCreatedConvert(resp){
   
    let topCreatedData = [];
    if( resp['aggregations']['2']['buckets'].length){
      topCreatedData.push(['Title', 'Top Created'])
    resp['aggregations']['2']['buckets'].forEach(item=>{
      topCreatedData.push([item.key, item.doc_count])
    })
    }

   
    return topCreatedData;
  }

  public topDeleted(){
    return this.http.post(`${config.nodeUri}/audit`, { "target": "topDeleted", "startDate": this.sharedService.dateObj['startDate'], "endDate": this.sharedService.dateObj['endDate'] })
    .map(res => this.topDeletedConvert(res.json())).toPromise();
  }

  topDeletedConvert(resp){
    let topDeletedData = [];
    if(resp['aggregations']['2']['buckets'].length){
      topDeletedData.push(['Title', 'Top Created'])
      resp['aggregations']['2']['buckets'].forEach(item=>{
        topDeletedData.push([item.key, item.doc_count])
      })
    }
    
    return topDeletedData;
  }

  public topUpdated(){
    return this.http.post(`${config.nodeUri}/audit`, { "target": "topUpdated", "startDate": this.sharedService.dateObj['startDate'], "endDate": this.sharedService.dateObj['endDate'] })
    .map(res => this.topUpdatedConvert(res.json())).toPromise();
  }

  topUpdatedConvert(resp){
    let topUpdatedData = [];
    if(resp['aggregations']['3']['buckets'].length){
      topUpdatedData.push(['Title', 'Top Updated'])
      resp['aggregations']['3']['buckets'].forEach(item=>{
        topUpdatedData.push([item.key, item.doc_count])
      })
    }
    
    return topUpdatedData;
  }

}
