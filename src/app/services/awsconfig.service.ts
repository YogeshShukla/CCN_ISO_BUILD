import { Component, Input, Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { config } from './config';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { SharedService } from '../services/shared.service';

@Injectable()
export class AwsconfigService {

  constructor(private http: Http, private sharedService: SharedService) { }

  public configImageId() {
    return this.http.post(`${config.nodeUri}/awsconfig`, { "target": "configImageId", "startDate": this.sharedService.dateObj['startDate'], "endDate": this.sharedService.dateObj['endDate'] })
      .map(res => this.configImageIdConvert(res.json())).toPromise();
  }

  configImageIdConvert(resp) {
    let configImageIdData = [];
    if (resp['aggregations']['2']['buckets'].length) {
      configImageIdData.push(['Title', 'Config Image Id'])
      resp['aggregations']['2']['buckets'].forEach(item => {
        configImageIdData.push([item.key, item.doc_count])
      })
    }

    return configImageIdData;
  }

  public deletedOnTermination() {
    return this.http.post(`${config.nodeUri}/awsconfig`, { "target": "deletedOnTermination", "startDate": this.sharedService.dateObj['startDate'], "endDate": this.sharedService.dateObj['endDate'] })
      .map(res => this.deletedOnTerminationConvert(res.json())).toPromise();
  }

  deletedOnTerminationConvert(resp) {
    let deletedOnTerminationData = [];
    if (resp['aggregations']['2']['buckets'].length) {
      deletedOnTerminationData.push(['Title', 'Volumes Deleted On Instance Termination'])
      resp['aggregations']['2']['buckets'].forEach(item => {
        deletedOnTerminationData.push([item.key_as_string == 'true' ? 'Deleted' : 'Not Deleted', item.doc_count])
      })
    }

    return deletedOnTerminationData;
  }

  public resourceType() {
    return this.http.post(`${config.nodeUri}/awsconfig`, { "target": "resourceType", "startDate": this.sharedService.dateObj['startDate'], "endDate": this.sharedService.dateObj['endDate'] })
      .map(res => this.resourceTypeConvert(res.json())).toPromise();
  }

  resourceTypeConvert(resp) {
    let resourceTypenData = [];
    if (resp['aggregations']['2']['buckets'].length) {
      resourceTypenData.push(['Title', 'Resource Type'])
      resp['aggregations']['2']['buckets'].forEach(item => {
        resourceTypenData.push([item.key, item.doc_count])
      })
    }


    return resourceTypenData;
  }

  public resourceCreation() {
    return this.http.post(`${config.nodeUri}/awsconfig`, { "target": "resourceCreation", "startDate": this.sharedService.dateObj['startDate'], "endDate": this.sharedService.dateObj['endDate'] })
      .map(res => this.resourceCreationConvert(res.json())).toPromise();
  }

  resourceCreationConvert(resp) {
    let resourceCreationData = [];
    resourceCreationData.push(['Title', 'Resource Creation per week'])
    resp['aggregations']['2']['buckets'].forEach(item=>{
      let dateStr = this.sharedService.dateTimeConverter(item['key_as_string']);

      resourceCreationData.push([dateStr, item.doc_count])
    })
    return resourceCreationData;


  }
}
