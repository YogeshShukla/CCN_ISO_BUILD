import { Component, Input, Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { config } from './config';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { SharedService } from '../services/shared.service';

@Injectable()
export class FinancialsService {


    constructor(private http: Http, private sharedService: SharedService) { }
    public ec2InstancesCost() {
        return this.http.post(`${config.nodeUri}/financials`, { "target": "ec2instanceCost", "startDate": this.sharedService.dateObj['startDate'], "endDate": this.sharedService.dateObj['endDate'] })
            .map(res => this.ec2InstancesCostConvert(res.json())).toPromise();
    }


    ec2InstancesCostConvert(resp) {
        let instanceObj = resp['aggregations']['2']['buckets']['all Amazon EC2 Instances']['3']['buckets'];
        let ec2InstancesCostdata = [];
        if (resp['aggregations']['2']['buckets']['all Amazon EC2 Instances']['1']['value']) {
           // ec2InstancesCostdata.push(['Title', 'EC2 Instances Cost']);

           //'AWS Account Total Cost' 
           // ec2InstancesCostdata.push(['AWS Account Total Cost', resp['aggregations']['2']['buckets']['all Amazon EC2 Instances']['1']['value']]);

           //AWS Account Total Cost
           let tot = 0;
           Object.keys(instanceObj).map(key => {
            tot+= instanceObj[key]['1']['value'];
           });

           let total = tot.toFixed(2);
            Object.keys(instanceObj).map(key => {
                ec2InstancesCostdata.push([key+' - $'+instanceObj[key]['1']['value'].toFixed(2), 'EC2 Instances Cost'+' - $'+total, instanceObj[key]['1']['value']])
                
            })
            ec2InstancesCostdata.push(['EC2 Instances Cost'+' - $'+total, 'AWS Account Total Cost' ,tot]);
            
        }
        return ec2InstancesCostdata;
    }


    public awsAccTotalCost() {
        return this.http.post(`${config.nodeUri}/financials`, { "target": "awsTotalCost", "startDate": this.sharedService.dateObj['startDate'], "endDate": this.sharedService.dateObj['endDate'] })
            .map(res => this.awsAccTotalCostConvert(res.json())).toPromise();
    }


    awsAccTotalCostConvert(resp) {
        let awsAccTotalCostData = [];
        if (resp['aggregations']['1']['value']) {
            awsAccTotalCostData.push(['Title', 'Parent', 'Size']);
            awsAccTotalCostData.push(['AWS Account Total Cost', null , resp['aggregations']['1']['value']]);
        }

        return awsAccTotalCostData;
    }


    public s3reqCost() {
        return this.http.post(`${config.nodeUri}/financials`, { "target": "s3reqCost", "startDate": this.sharedService.dateObj['startDate'], "endDate": this.sharedService.dateObj['endDate'] })
            .map(res => this.s3reqCostConver(res.json())).toPromise();
    }


    s3reqCostConver(resp) {
        let s3reqCostData = [];
       // s3reqCostData.push(['Title', 'Amazon S3 Request Cost']);
        let respArray = resp['aggregations']['2']['buckets']['AmazonS3']['3']['buckets'];
        let tot = 0;
        respArray.forEach(item => {
            tot+= item['1']['value']
        })
        let total = tot.toFixed(2);
        respArray.forEach(item => {
            s3reqCostData.push([item.key+' - $'+item['1']['value'].toFixed(2),'Amazon S3 Request Cost'+' - $'+total, item['1']['value']])
            
        })
        s3reqCostData.push(['Amazon S3 Request Cost'+' - $'+total, 'AWS Account Total Cost',tot])
        return s3reqCostData;
    }


    public separateServiceCost() {
        return this.http.post(`${config.nodeUri}/financials`, { "target": "separateServiceCost", "startDate": this.sharedService.dateObj['startDate'], "endDate": this.sharedService.dateObj['endDate'] })
            .map(res => this.serviceCostConver(res.json())).toPromise();
    }
    serviceCostConver(resp) {
        let serviceCostConverData = [];
        //serviceCostConverData.push(['Title', 'Separate Service Cost']);
        let respArray = resp['aggregations']['2']['buckets'];
        respArray.forEach(item => {
            serviceCostConverData.push([item.key +' - $'+item['1']['value'].toFixed(2), 'AWS Account Total Cost',item['1']['value']])
        })
        return serviceCostConverData;
    }

    public financials(callback){
        Promise.all([this.awsAccTotalCost(), this.ec2InstancesCost(),  this.s3reqCost(), this.separateServiceCost()]).then(values => {
           
            let consolidatedArray = [];
            let awsTotalCost = 0;
            values[0].forEach(item=>{
                if(item[0]=='AWS Account Total Cost'){
                    awsTotalCost=item[2];
                }
            })
            let x=0;
            values.forEach(item=>{
                let y=0;
                
                item.forEach(subItem=>{
                        let z=0;
                        subItem.forEach(innerItem=>{
                            if(innerItem=='AWS Account Total Cost'){
                                values[x][y][z]= 'AWS Account Total Cost'+' - $'+awsTotalCost.toFixed(2);
                            }
                            z++;
                        })
                        
                    y++;
                });
             
                consolidatedArray= consolidatedArray.concat(values[x]);
                x++;
            })
          
            callback(consolidatedArray)
          })
          .catch(error => {
            console.error("ERROR... " + error);
          });
    }
}