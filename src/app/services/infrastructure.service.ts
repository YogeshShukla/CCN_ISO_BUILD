import { Component, Input, Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { config } from './config';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import {SharedService} from './shared.service'

@Injectable()
export class InfrastructureService {

  constructor(private http: Http, private sharedService:SharedService) {}
  public cpu() {
    return this.http.post(`${config.nodeUri}/infrastructure`,{"target":"cpu", "startDate": this.sharedService.dateObj['startDate'], "endDate": this.sharedService.dateObj['endDate']})
      .map(res => this.convertCpuData(res.json())).toPromise();
  }

  convertCpuData(resp) {

    let cpuData = [];
    cpuData.push(['Title', 'CPU']);
    let k = resp.hits.hits[0]._source.system.cpu;
    const cpu_arr = Object.keys(k).map(i => {
      if (k[i]['pct'])
        cpuData.push([i.charAt(0).toUpperCase() + i.slice(1), k[i]['pct']])
    })
    return cpuData;
  }

  public storage() {
    return this.http.post(`${config.nodeUri}/infrastructure`,{"target":"fsstat", "startDate": this.sharedService.dateObj['startDate'], "endDate": this.sharedService.dateObj['endDate']})
      .map(res => this.convertStorageData(res.json())).toPromise();
  }

  convertStorageData(resp) {
    let storageData = [];
    storageData.push(['Title', 'Disk (In MB)']);
    let k = resp.hits.hits[0]._source.system.fsstat.total_size;
    const storage_arr = Object.keys(k).map(i => {
      storageData.push([i.charAt(0).toUpperCase() + i.slice(1), parseFloat(k[i]) / 1000000])
    })
    return storageData;
  }

  public network() {
    return this.http.post(`${config.nodeUri}/infrastructure`,{"target":"network", "startDate": this.sharedService.dateObj['startDate'], "endDate": this.sharedService.dateObj['endDate']})
      .map(res => this.convertNetworkData(res.json())).toPromise();
  }

  convertNetworkData(resp) {
    let networkData = [];
    networkData.push(['Title', 'In', 'Out']);

    let memory_assets = ['errors', 'dropped', 'bytes', 'packets']

    memory_assets.forEach(item => {
      if (resp.hits.hits[0]._source.system.network.in[item] && resp.hits.hits[0]._source.system.network.in[item]) {
        networkData.push([item, resp.hits.hits[0]._source.system.network.in[item], resp.hits.hits[0]._source.system.network.out[item]])
      }
    })
    return networkData;
  }

  public memory() {
    return this.http.post(`${config.nodeUri}/infrastructure`,{"target":"memory", "startDate": this.sharedService.dateObj['startDate'], "endDate": this.sharedService.dateObj['endDate']})
      .map(res => this.convertMemoryData(res.json())).toPromise();
  }

  convertMemoryData(resp) {
    let memoryData = [];
    memoryData.push(['Title', 'Memory (In MB)']);
    memoryData.push(['Free', parseFloat(resp.hits.hits[0]._source.system.memory.free)/1000000]);
    memoryData.push(['Total', parseFloat(resp.hits.hits[0]._source.system.memory.total)/1000000]);
    memoryData.push(['Used', parseFloat(resp.hits.hits[0]._source.system.memory.used.bytes)/1000000]);
    return memoryData;
  }

}
