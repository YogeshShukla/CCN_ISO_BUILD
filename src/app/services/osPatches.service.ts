import { Component, Input, Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { config } from './config';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/forkJoin';


import { SharedService } from './shared.service'

@Injectable()
export class osPatchesService {
  constructor(private http: Http, private sharedService: SharedService) { }
  
patchesApi(){
  return this.http.get(`${config.nodeUri}/osPatches`)
  .map((res) => res.json()).toPromise();
}

}










  


