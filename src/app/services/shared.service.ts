import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class SharedService {
  dateObj: object = { startDate: new Date(2018, 0, 1, 0, 0).getTime(), endDate: new Date().getTime() };
  public bs = new BehaviorSubject<Object>({ startDate: new Date(2018, 0, 1, 0, 0).getTime(), endDate: new Date().getTime() });
  public myObserver = this.bs.asObservable();
  constructor() {
    this.bs.subscribe(item => {
      this.dateObj = item;
    })
  }

  newDateRangeSelected(dateObj) {
    this.bs.next(dateObj);
  }

  dateTimeConverter(datetime) {
    let dateObj = new Date(datetime);
    let date = dateObj.getDate().toString().length < 2 ? '0' + dateObj.getDate() : dateObj.getDate();
    let month = (dateObj.getMonth() + 1).toString().length < 2 ? '0' + (dateObj.getMonth() + 1) : (dateObj.getMonth() + 1);
    let year = dateObj.getFullYear();

    let hrs = String(dateObj.getHours()).length < 2 ? '0' + dateObj.getHours() : dateObj.getHours();
    let mins = dateObj.getMinutes().toString().length < 2 ? '0' + dateObj.getMinutes() : dateObj.getMinutes();
    let time = hrs + ':' + mins;
    let dateStr = date + '-' + month + '-' + year;

    return dateStr;
  }

  timeConverter(datetime) {
    let dateObj = new Date(datetime);
    let date = dateObj.getDate().toString().length < 2 ? '0' + dateObj.getDate() : dateObj.getDate();
    let month = (dateObj.getMonth() + 1).toString().length < 2 ? '0' + (dateObj.getMonth() + 1) : (dateObj.getMonth() + 1);
    let year = dateObj.getFullYear();

    let hrs = String(dateObj.getHours()).length < 2 ? '0' + dateObj.getHours() : dateObj.getHours();
    let mins = dateObj.getMinutes().toString().length < 2 ? '0' + dateObj.getMinutes() : dateObj.getMinutes();
    let time = hrs + ':' + mins;
    let dateStr = date + '-' + month + '-' + year;

    return time;
  }

}
