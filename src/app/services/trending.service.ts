import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { config } from './config';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { SharedService } from '../services/shared.service';

@Injectable()
export class TrendingService {
  constructor(private http: Http, private sharedService: SharedService) { }
  public topHostsCpu() {
    return this.http.post(`${config.nodeUri}/trending`, { "target": "topHostsCpu", "startDate": this.sharedService.dateObj['startDate'], "endDate": this.sharedService.dateObj['endDate'] })
      .map(res => this.trendingConverter(res.json(), 'Top Hosts By CPU for System')).toPromise();

  }

  topHostsMemory() {
    return this.http.post(`${config.nodeUri}/trending`, { "target": "topHostsMemory", "startDate": this.sharedService.dateObj['startDate'], "endDate": this.sharedService.dateObj['endDate'] })
      .map(res => this.trendingConverter(res.json(), 'Top Hosts By Memory for System')).toPromise();
  }

  topProcessCpu() {
    return this.http.post(`${config.nodeUri}/trending`, { "target": "topProcessCpu", "startDate": this.sharedService.dateObj['startDate'], "endDate": this.sharedService.dateObj['endDate'] })
      .map(res => this.trendingConverter(res.json(), 'Top Processes By CPU')).toPromise();
  }


  topProcessMemory() {
    return this.http.post(`${config.nodeUri}/trending`, { "target": "topProcessMemory", "startDate": this.sharedService.dateObj['startDate'], "endDate": this.sharedService.dateObj['endDate'] })
      .map(res => this.trendingConverter(res.json(), 'Top Process By Memory')).toPromise();
  }

  trendingConverter(resp, title) {

    let infoData = [];

    let level1Date = new Date();
    level1Date.setMinutes(level1Date.getMinutes() - 15);

    let level2Date = new Date();
    level2Date.setMinutes(level2Date.getMinutes() - 10);

    let level3Date = new Date();
    level3Date.setMinutes(level3Date.getMinutes() - 5);

    let titles = ['Dates'];

    let content = JSON.parse(resp.body);
    
    titles.push(this.sharedService.timeConverter(level2Date));
    titles.push(this.sharedService.timeConverter(level3Date));
    titles.push(this.sharedService.timeConverter(new Date()));


    infoData.push(titles);

    content[Object.keys(JSON.parse(resp.body))[0]]['series'].forEach(item => {
      let fileds = [item.label];
      let count = 0;
      let value = 0;

      item.data.forEach(innerItem => {

        if (innerItem[1] != null && innerItem[0] > level1Date.getTime() && innerItem[0] < level2Date.getTime()) {
          value += innerItem[1];
          count++;
        }

      });
      let calculateAvgValue1 = isNaN((value / count) * 100) ? 0 : (value / count) * 100;
      fileds.push(calculateAvgValue1);

      count = 0;
      value = 0;

      item.data.forEach(innerItem => {
        if (innerItem[1] != null && innerItem[0] > level2Date.getTime() && innerItem[0] < level3Date.getTime()) {
          value += innerItem[1];
          count++;
        }

      });

      let calculateAvgValue2 = isNaN((value / count) * 100) ? 0 : (value / count) * 100;
      fileds.push(calculateAvgValue2);

      count = 0;
      value = 0;

      item.data.forEach(innerItem => {
        if (innerItem[1] != null && innerItem[0] > level3Date.getTime() && innerItem[0] < new Date().getTime()) {
          value += innerItem[1];
          count++;
        }

      });

      let calculateAvgValue3 = isNaN((value / count) * 100) ? 0 : (value / count) * 100;
      fileds.push(calculateAvgValue3);

      infoData.push(fileds);


    });

    return this.transposeArray(infoData);

  }
/*
  trendingConverterAll(resp,title) {
    
    let infoData = [];


    let titles = ['Dates'];
   
      let content = JSON.parse(resp.body);
      content[Object.keys(JSON.parse(resp.body))[0]]['series'][0]['data'].forEach(item=>{
        if(item[1]!=null){
          titles.push(this.sharedService.timeConverter(item[0]));
        }
        
      })

      infoData.push(titles);


    content[Object.keys(JSON.parse(resp.body))[0]]['series'].forEach(item => {
      let fileds = [item.label];
      
      item.data.forEach(innerItem=>{
        if(innerItem[1]!=null || titles.indexOf(innerItem[0])!=-1){
            if(innerItem[1]!=null && fileds.length!=0){
              fileds.push(parseFloat(innerItem[1]));
            }
            else{
              fileds.push(innerItem[1]);
            }
        }
      })
      infoData.push(fileds);
    })

    return this.transposeArray(infoData);
  }
  */

  transposeArray(array) {
    var newArray = [];
    for (var i = 0; i < array[0].length; i++) {
      newArray.push([]);
    };

    for (var i = 0; i < array.length; i++) {
      for (var j = 0; j < array[0].length; j++) {
        newArray[j].push(array[i][j]);
      };
    };

    return newArray;
  }
}


